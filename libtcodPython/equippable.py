__author__ = 'darithorn'

import libtcodpy as libtcod
import object, vec2

equippableTypes = {"Unknown": 0, "Weapon": 1, "Armor": 2, "Shield": 3, "Ammunition": 4}
weaponTypes = {"Melee": 0, "Ranged": 1}
ammunitionTypes = {"Arrow": 0, "Bolt": 1}
armorTypes = {"Hands": 0, "Legs": 1, "Head": 2, "Torso": 3, "Feet": 4}


class Requirements():
    def __init__(self, str=0, dex=0, int=0, wpr=0, twoH=False, stam=0, amm=None):
        self.str = str
        self.dex = dex
        self.int = int
        self.wpr = wpr
        self.stam = stam
        self.twoH = twoH
        self.amm = amm

    def copy(self):
        return Requirements(self.str, self.dex, self.int, self.wpr, self.twoH, self.stam, self.amm)


class Stats():
    def __init__(self, str=0, dex=0, int=0, wpr=0, per=0, spd=0, # Attributes
                 rP=0, rF=0, rC=0, rT=0, # Resists
                 ac=0, blockChance=0, # Armor / Shields
                 minDmg=0, maxDmg=0): # Weapons
        self.str = str
        self.dex = dex
        self.int = int
        self.wpr = wpr
        self.per = per
        self.spd = spd
        self.rP = rP
        self.rF = rF
        self.rC = rC
        self.rT = rT
        self.ac = ac
        self.blockChance = blockChance
        self.minDmg = minDmg
        self.maxDmg = maxDmg

    def copy(self):
        return Stats(self.str, self.dex, self.int, self.wpr, self.per, self.spd, self.rP, self.rF, self.rC, self.rT,
                     self.ac, self.blockChance, self.minDmg, self.maxDmg)


class Equippable(object.Object):
    def __init__(self, char, color, emap, desc, name, eType, reqs, stats, backgroundColor=libtcod.black,
                 stackable=False, amount=1, pluralName="", spawner=False):
        object.Object.__init__(self, vec2.Vec2(0, 0), char, color, object.objectTypes["Equippable"], emap, desc, name,
                               backgroundColor, stackable=stackable, amount=amount, pluralName=pluralName,
                               spawner=spawner)
        self.realName = self.name
        self.eType = eType
        self.equipped = False
        self.reqs = reqs
        self.stats = stats
        self.level = 1
        self.statLevels = {1: self.stats}
        self.reqLevels = {1: self.reqs}

        count = 2
        while count < 16:
            reqs, stats = self.createLevel(count)
            self.statLevels.update(({count: stats}))
            self.reqLevels.update(({count: reqs}))
            count += 1

        self.stats = self.statLevels[1]
        self.reqs = self.reqLevels[1]

    def copy(self, pos, emap):
        i = Equippable(self.char, self.color, emap, self.desc, self.name, self.eType, self.reqs, self.stats,
                       self.backgroundColor, self.amount)
        i.pos = pos
        return i

    def setLevel(self, level):
        self.stats = self.statLevels[level].copy()
        self.reqs = self.reqLevels[level].copy()
        self.level = level
        if self.level != 1:
            self.name = "+" + str(self.level) + " " + self.name
        else:
            self.name = self.realName

    def createLevel(self, level):
        reqs = Requirements(self.reqLevels[1].str, self.reqLevels[1].dex, self.reqLevels[1].int, self.reqLevels[1].wpr,
                            self.reqLevels[1].twoH, self.reqLevels[1].stam, self.reqLevels[1].amm)

        stats = Stats(self.statLevels[1].str, self.statLevels[1].dex, self.statLevels[1].int, self.statLevels[1].wpr,
                      self.statLevels[1].per, self.statLevels[1].spd, self.statLevels[1].rP, self.statLevels[1].rF,
                      self.statLevels[1].rC, self.statLevels[1].rT,
                      self.statLevels[1].ac, self.statLevels[1].blockChance, self.statLevels[1].minDmg,
                      self.statLevels[1].maxDmg)

        reqs.str *= level
        reqs.dex *= level
        reqs.int *= level
        reqs.stam *= level

        stats.str *= level
        stats.dex *= level
        stats.int *= level
        stats.wpr *= level
        stats.per *= level
        stats.spd *= level
        stats.rP *= level
        stats.rF *= level
        stats.rC *= level
        stats.rT *= level
        stats.ac *= level
        stats.minDmg *= level
        stats.maxDmg *= level
        stats.blockChance *= level
        if stats.blockChance > 70: stats.blockChance = 70

        return reqs, stats


class Weapon(Equippable):
    def __init__(self, color, emap, desc, name, reqs, stats, weaponType=weaponTypes["Melee"],
                 backgroundColor=libtcod.black, spawner=False):
        Equippable.__init__(self, '/', color, emap, desc, name, equippableTypes["Weapon"], reqs, stats, backgroundColor,
                            spawner=spawner)
        self.weaponType = weaponType

    def copy(self, pos, emap):
        i = Weapon(self.color, emap, self.desc, self.name, self.reqs, self.stats, self.weaponType, self.backgroundColor)
        i.pos = pos
        return i


class Armor(Equippable):
    def __init__(self, color, emap, desc, name, reqs, stats, armorType, backgroundColor=libtcod.black, spawner=False):
        Equippable.__init__(self, '[', color, emap, desc, name, equippableTypes["Armor"], reqs, stats, backgroundColor,
                            spawner=spawner)
        self.armorType = armorType

    def copy(self, pos, emap):
        i = Armor(self.color, emap, self.desc, self.name, self.reqs, self.stats, self.armorType, self.backgroundColor)
        i.pos = pos
        return i


class Shield(Equippable):
    def __init__(self, color, emap, desc, name, reqs, stats, backgroundColor=libtcod.black, spawner=False):
        Equippable.__init__(self, ')', color, emap, desc, name, equippableTypes["Shield"], reqs, stats, backgroundColor,
                            spawner=spawner)

    def copy(self, pos, emap):
        i = Shield(self.color, emap, self.desc, self.name, self.reqs, self.stats, self.backgroundColor)
        i.pos = pos
        return i


class Ammunition(Equippable):
    def __init__(self, color, emap, desc, name, reqs, stats, ammoType, backgroundColor=libtcod.black, amount=1,
                 pluralName="", spawner=False):
        Equippable.__init__(self, '!', color, emap, desc, name, equippableTypes["Ammunition"], reqs, stats,
                            backgroundColor, stackable=True, amount=amount, pluralName=pluralName, spawner=spawner)
        self.ammoType = ammoType

    def copy(self, pos, emap):
        i = Ammunition(self.color, emap, self.desc, self.name, self.reqs, self.stats, self.ammoType,
                       self.backgroundColor,
                       self.amount, self.pluralName, False)
        i.pos = pos
        return i
