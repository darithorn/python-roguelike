__author__ = 'darithorn'

import libtcodpy as libtcod

entityTypes = {"Unknown": 0, "Object": 1, "Mob": 2, "Tile": 3, "Empty": 4}


class Entity():
    def __init__(self, pos, char, color, entityType, emap, desc, blocking=False, blockSight=False,
                 backgroundColor=libtcod.black):
        self.pos = pos
        self.char = char
        self.color = color
        self.type = entityType
        self.emap = emap
        self.desc = desc
        self.blocking = blocking
        self.blockSight = blockSight
        self.backgroundColor = backgroundColor

    def setPos(self, x, y):
        """
        Use move() instead for AI
        """
        self.emap.resetProperty(self)
        self.pos.x = x
        self.pos.y = y
        self.emap.setProperty(self)

    def update(self):
        pass

    def draw(self):
        libtcod.console_set_default_background(0, self.backgroundColor)
        libtcod.console_set_default_foreground(0, self.color)
        libtcod.console_put_char(0, self.pos.x, self.pos.y, self.char, libtcod.BKGND_SET)


class Empty(Entity):
    def __init__(self, pos, emap):
        Entity.__init__(self, pos, '#', libtcod.dark_gray, entityTypes["Empty"], emap, "")
