__author__ = 'darithorn'

import libtcodpy as libtcod
import entity, vec2, ui, object, utility
import random, math

mobTypes = {"Unknown": 0, "Player": 1, "Flee": 2, "Normal": 3}


class Mob(entity.Entity):
    def __init__(self, pos, char, color, mobType, emap, desc, name, spawner=False, blockSight=False,
                 backgroundColor=libtcod.black):
        entity.Entity.__init__(self, pos, char, color, entity.entityTypes["Mob"], emap, desc, False, blockSight,
                               backgroundColor)
        self.blocking = True
        self.spawner = spawner
        self.name = name
        self.mobType = mobType

        self.dead = False
        self.doneFor = False

        self.maxhp = 1.0
        self.maxstam = 1.0
        self.maxmp = 1.0

        self.hp = self.maxhp
        self.stam = self.maxstam
        self.mp = self.maxmp

        self.level = 1

        self.poisonResist = 0
        self.fireResist = 0
        self.coldResist = 0
        self.taintResist = 0
        self.taint = 0
        self.str = 1
        self.dex = 1
        self.int = 1
        self.wpr = 1
        self.perception = 50 # so mutations can lower perception
        self.speed = 1  # min. 1 or mob cannot move
        self.moves = 0
        self.ableToMove = True

        self.stamUse = 0

        self.mPmin = 0
        self.mPmax = 0
        self.mFmin = 0
        self.mFmax = 0
        self.mCmin = 0
        self.mCmax = 0
        self.mTmin = 0
        self.mTmax = 0
        self.basemindmg = 0
        self.basemaxdmg = 1
        self.wepmindmg = 0
        self.wepmaxdmg = 0
        self.mindmg = self.basemindmg + self.wepmindmg
        self.maxdmg = self.basemaxdmg + self.wepmaxdmg

        self.armorClass = 0
        self.blockChance = 0
        self.hasRangedWep = False
        self.hitChance = 10
        self.dodgeChance = 0
        self.attackRange = 1 # 1.4 is melee >1 is ranged
        self.hasRangedWep = False
        self.ammoType = -1

        if not self.spawner:
            self.emap.addMob(self)

    def move(self, dx, dy):
        """
        Use this for mob movement
        """
        self.moves += 1
        tempX = self.pos.x + dx
        tempY = self.pos.y + dy
        tile = self.emap.getTile(vec2.Vec2(tempX, tempY))
        if tile.blocking: return True
        mob = self.emap.getMobs(vec2.Vec2(tempX, tempY))
        if mob is not None: return False
        if tempX < 0 or tempX > self.emap.width: return True
        if tempY < 0 or tempY > self.emap.height: return True
        self.emap.resetProperty(self)
        self.pos.x = tempX
        self.pos.y = tempY
        self.emap.setProperty(self)

    def go(self):
        self.moves = 0
        self.ableToMove = True

    def update(self):
        if self.moves < self.speed:
            self.ableToMove = True
        else:
            self.ableToMove = False

    def updateStats(self):
        self.basemindmg = int(self.str / 4 - 1)
        if self.basemindmg < 0: self.basemindmg = 0
        self.basemaxdmg = int(round(self.str / 3, 0))
        self.mindmg = self.basemindmg + self.wepmindmg
        self.maxdmg = self.basemaxdmg + self.wepmaxdmg
        self.maxhp = float(self.str * 3.5 + 5)
        self.maxstam = float((self.dex * 2.8 + 3) + (self.str * 1.3))
        self.maxmp = float((self.int * 3) + (self.wpr * 1.2))
        self.speed = int(self.dex * 0.1)
        self.hitChance = int((self.dex * 10) + (self.str * 6))
        if self.hitChance < 10: self.hitChance = 10
        if self.hitChance > 90: self.hitChance = 90
        self.dodgeChance = int((self.dex * 4) / 1.5)
        if self.speed < 1: self.speed = 1

    def dist(self, mob):
        return self.pos.distance(mob.pos)

    def missed(self):
        ui.appendln("The " + self.name + " misses.")

    def dodged(self, mob):
        ui.appendln("The " + self.name + " dodged your attack.")

    def hit(self, amount, mob):
        ui.appendln("The " + self.name + " hit you for " + str(amount) + " damage.", libtcod.desaturated_red)

    def blocked(self, mob):
        ui.appendln("The " + self.name + " blocked your attack")

    def die(self):
        ui.appendln("The " + self.name + " dies!", libtcod.green)
        expGiven = (self.level - self.emap.fovPlayer.level) + 2
        if expGiven < 1: expGiven = 1
        self.emap.fovPlayer.giveExp(expGiven)
        self.emap.addObj(object.Corpse(self.pos, self.color, self.emap))
        self.emap.removeMob(self)
        self.ableToMove = False
        self.doneFor = True
        utility.rm.mobSpawner.monsterAmount -= 1

    def tired(self):
        pass

    def damage(self, amount):
        self.hp -= int(amount)
        if int(self.hp) <= 0:
            self.dead = True

    def attack(self, mob):
        self.moves += 1

        if self.stam < self.stamUse: # stam check
            self.tired()
            return

        hit = random.randint(0, 100)
        if self.hasRangedWep:
            hit += self.dist(mob) * 1.5
            hit -= int(self.dex * 2)
        if hit > self.hitChance: # hit chance check
            self.missed()
            return

        if random.randint(0, 100) < mob.dodgeChance: # dodge check
            mob.dodged(self)
            return

        if random.randint(0, 100) < mob.blockChance: # block chance check
            mob.blocked(self)
            return

        damage = random.randint(self.mindmg, self.maxdmg)
        acvoid = random.randint(0, mob.armorClass)
        gdr = (5 * math.pow(mob.armorClass, 0.68)) / 100
        #if gdr > 0.5: gdr = 0.5
        damage -= acvoid + (damage * gdr)
        damage = int(round(damage, 0))
        self.stam -= self.stamUse
        if damage > 0:
            mob.damage(damage)
            self.hit(damage, mob)

        # Magic damage
        mPdam = random.randint(self.mPmin, self.mPmax)
        if mPdam > 0:
            mob.damage(mPdam)
            self.hit(mPdam, mob)

        mFdam = random.randint(self.mFmin, self.mFmax)
        if mFdam > 0:
            mob.damage(mFdam)
            self.hit(mFdam, mob)

        mCdam = random.randint(self.mCmin, self.mCmax)
        if mCdam > 0:
            mob.damage(mCdam)
            self.hit(mCdam, mob)

        mTdam = random.randint(self.mTmin, self.mTmax)
        if mTdam > 0:
            mob.damage(mTdam)
            self.hit(mTdam, mob)

        # Always has player position
        # Contains personal FOV

        # Alert Behaviour
        # If player is in FOV
        # set last known position
        # Chase

        # Chase Behaviour
        # while player is not next to mob
        # if player has moved from last known position
        # if player is in FOV
        # recalculate path
        # walk along path
        # if player is inside attack range
        # Attack
        # if health is lower than flee threshold
        # Flee
        # set last known position

        # Attack Behaviour
        # while player is inside attack range
        # attack()
        # if health is lower than flee threshold
        # Flee
        # set last known position
        # if player is not next to mob
        # Chase

        # Flee Behaviour
        # while health is lower than flee threshold
        # walk opposite direction from player
        # if health is above flee threshold
        # Alert


behaviours = {"Alert": 0, "Chase": 1, "Attack": 2, "Flee": 3}


class NormalMob(Mob):
    def __init__(self, pos, char, color, emap, desc, name, spawner=False, blockSight=False,
                 backgroundColor=libtcod.black):
        Mob.__init__(self, pos, char, color, mobTypes["Normal"], emap, desc, name, spawner, blockSight, backgroundColor)
        self.behaviour = behaviours["Alert"]
        self.lastKnownPos = self.pos
        self.fov = libtcod.map_new(self.emap.width, self.emap.height)
        libtcod.map_copy(self.emap.fov, self.fov)
        #self.path = libtcod.path_new_using_map(self.fov, 1)
        self.path = libtcod.dijkstra_new(self.fov)
        self.inFOV = False
        self.isPlayerInFOV()
        self.isPath = True
        self.blockPos = None
        self.randomColor = libtcod.Color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

    def copy(self, pos, emap):
        m = NormalMob(pos, self.char, self.color, emap, self.desc, self.name, False, self.blockSight,
                      self.backgroundColor)
        m.attackRange = self.attackRange
        m.str = self.str
        m.dex = self.dex
        m.int = self.int
        m.wpr = self.wpr
        m.level = self.level
        m.armorClass = self.armorClass
        m.wepmindmg = self.wepmindmg
        m.wepmaxdmg = self.wepmaxdmg
        m.updateStats()
        m.hp = m.maxhp
        m.stam = m.maxstam
        m.mp = m.maxmp
        return m

    def isPlayerInFOV(self):
        libtcod.map_compute_fov(self.fov, self.pos.x, self.pos.y, self.perception, True, libtcod.FOV_PERMISSIVE_3)
        self.inFOV = libtcod.map_is_in_fov(self.fov, self.emap.fovPlayer.pos.x, self.emap.fovPlayer.pos.y)
        return self.inFOV

    def computePath(self):
        self.isPath = True
        libtcod.dijkstra_compute(self.path, self.pos.x, self.pos.y)
        if not libtcod.dijkstra_path_set(self.path, self.lastKnownPos.x, self.lastKnownPos.y):
            self.isPath = False
        #if not libtcod.path_compute(self.path, self.pos.x, self.pos.y, self.emap.fovPlayer.pos.x, self.emap.fovPlayer.pos.y):
        #    self.isPath = False

    def checkPath(self):
        if not libtcod.dijkstra_is_empty(self.path):
            count = 0
            x, y = libtcod.dijkstra_get(self.path, count)
            while x < self.emap.width and y < self.emap.height:
                if not vec2.Vec2(x, y).equals(self.emap.fovPlayer.pos) and not vec2.Vec2(x, y).equals(
                        self.pos) and not libtcod.map_is_walkable(self.fov, x, y):
                    self.blockPos = vec2.Vec2(x, y)
                    return False
                count += 1
                x, y = libtcod.dijkstra_get(self.path, count)
            self.blockPos = None
            return True
        else:
            return False

    def move(self, dx, dy):
        """
        Use this for mob movement
        """
        self.moves += 1
        tempX = self.pos.x + dx
        tempY = self.pos.y + dy
        tile = self.emap.getTile(vec2.Vec2(tempX, tempY))
        if tile.blocking: return
        mob = self.emap.getMobs(vec2.Vec2(tempX, tempY))
        if mob is not None: return
        if tempX < 0 or tempX > self.emap.width: return
        if tempY < 0 or tempY > self.emap.height: return
        self.emap.resetProperty(self)
        self.pos.x = tempX
        self.pos.y = tempY
        self.emap.setProperty(self)
        libtcod.map_compute_fov(self.fov, self.pos.x, self.pos.y, self.perception, True, libtcod.FOV_PERMISSIVE_3)

    #def draw(self):
    #    if not libtcod.dijkstra_is_empty(self.path):
    #        count = 0
    #        x, y = libtcod.dijkstra_get(self.path, count)
    #        while x < self.emap.width and y < self.emap.height:
    #            libtcod.console_set_default_background(0, self.randomColor)
    #            libtcod.console_put_char(0, x, y, ' ', libtcod.BKGND_SET)
    #            count += 1
    #            x, y = libtcod.dijkstra_get(self.path, count)
    #    if self.blockPos is not None:
    #        libtcod.console_set_default_background(0, libtcod.red)
    #        libtcod.console_put_char(0, self.blockPos.x, self.blockPos.y, ' ', libtcod.BKGND_SET)
    #    Mob.draw(self)

    def update(self):
        if self.spawner:
            self.ableToMove = False
            return

        if self.doneFor:
            return

        if self.dead:
            self.die()
            return

        if self.moves < self.speed:
            self.ableToMove = True
        else:
            self.ableToMove = False

        if self.ableToMove:

            # Should be able to move at beginning of each round
            if self.isPlayerInFOV(): # Check if the player is in FOV
                # If player is in FOV and our last known position hasn't been updated
                # Update it

                if not self.lastKnownPos.equals(self.emap.fovPlayer.pos):
                    self.lastKnownPos = self.emap.fovPlayer.pos.copy()
                    # Since last known position has been updated
                    # Recompute the path
                    self.computePath()
            elif self.pos.equals(self.lastKnownPos):
                # At last known position and cannot see player
                # Go back to Alert
                self.behaviour = behaviours["Alert"]
                self.ableToMove = False
                return

            if self.behaviour == behaviours["Alert"]:
                self.alert()
            elif self.behaviour == behaviours["Chase"]:
                self.chase()
            elif self.behaviour == behaviours["Attack"]:
                self.attackBehaviour()

    def insideAttackRange(self, pos):
        if self.attackRange == 1:
            return self.pos.nextTo(pos)
        else:
            return self.pos.distance(self.lastKnownPos) <= self.attackRange

    def alert(self):
        if self.inFOV:
            # Player is in FOV
            # Last Known Position should've been set before this
            # And a path computed
            self.computePath()
            self.behaviour = behaviours["Chase"]
        else:
            self.ableToMove = False
            # Can't see the player. Can't do anything.

    def chase(self):
        if not self.insideAttackRange(self.emap.fovPlayer.pos):
            #if not self.checkPath():
                # If something is blocking the path e.g. a mob
                # Recompute the path
            #    self.computePath()
            x, y = libtcod.dijkstra_path_walk(self.path)
            if x is None or y is None or x > self.emap.width or y > self.emap.height:
                # Something seems to be wrong with the path
                # Cannot move. Recompute path.
                self.ableToMove = False
            #    self.computePath()
                return
            # Subtract (x, y) from our position to get direction to move in
            self.move(x - self.pos.x, y - self.pos.y)
                # A mob is in our way
                # Recompute path to attempt to find a path around it
            #    self.computePath()
        else:
            # Player is inside attack range
            self.behaviour = behaviours["Attack"]

    def attackBehaviour(self):
        if self.insideAttackRange(self.emap.fovPlayer.pos):
            self.attack(self.emap.fovPlayer)
        else:
            if self.inFOV:
                self.computePath()
                self.behaviour = behaviours["Chase"]
            else:
                self.behaviour = behaviours["Alert"]