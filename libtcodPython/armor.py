__author__ = 'darithorn'

import libtcodpy as libtcod
import equippable


class Rags(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.brass, emap,
                                  "no desc",
                                  "Rags",
                                  equippable.Requirements(),
                                  equippable.Stats(ac=0),
                                  equippable.armorTypes["Torso"],
                                  spawner=spawner)

# Head
# ===================================
class HeadCloth(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Cloth Hat",
                                  equippable.Requirements(str=1),
                                  equippable.Stats(ac=1),
                                  equippable.armorTypes["Head"],
                                  spawner=spawner)


class HeadLeather(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Leather Cap",
                                  equippable.Requirements(str=3),
                                  equippable.Stats(ac=2),
                                  equippable.armorTypes["Head"],
                                  spawner=spawner)


class HeadChain(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Chain Coif",
                                  equippable.Requirements(str=7),
                                  equippable.Stats(ac=3),
                                  equippable.armorTypes["Head"],
                                  spawner=spawner)


class HeadPlate(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Plate Helmet",
                                  equippable.Requirements(str=18),
                                  equippable.Stats(per=-15, ac=4),
                                  equippable.armorTypes["Head"],
                                  spawner=spawner)

# Torso
# ===================================
class TorsoCloth(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Cloth Shirt",
                                  equippable.Requirements(str=1),
                                  equippable.Stats(ac=1),
                                  equippable.armorTypes["Torso"],
                                  spawner=spawner)


class TorsoLeather(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Leather Tunic",
                                  equippable.Requirements(str=4),
                                  equippable.Stats(ac=3),
                                  equippable.armorTypes["Torso"],
                                  spawner=spawner)


class TorsoChain(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Chain Hauberk",
                                  equippable.Requirements(str=9),
                                  equippable.Stats(dex=-1, ac=6),
                                  equippable.armorTypes["Torso"],
                                  spawner=spawner)


class TorsoPlate(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Plate Cuirass",
                                  equippable.Requirements(str=20),
                                  equippable.Stats(dex=-2, ac=10),
                                  equippable.armorTypes["Torso"],
                                  spawner=spawner)

# Hands
# ===================================
class HandsCloth(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Cloth Mittens",
                                  equippable.Requirements(str=1),
                                  equippable.Stats(ac=1),
                                  equippable.armorTypes["Hands"],
                                  spawner=spawner)


class HandsLeather(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Leather Gloves",
                                  equippable.Requirements(str=2),
                                  equippable.Stats(ac=2),
                                  equippable.armorTypes["Hands"],
                                  spawner=spawner)


class HandsChain(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Chain Mittens",
                                  equippable.Requirements(str=7),
                                  equippable.Stats(ac=3),
                                  equippable.armorTypes["Hands"],
                                  spawner=spawner)


class HandsPlate(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Plate Gauntlets",
                                  equippable.Requirements(str=14),
                                  equippable.Stats(dex=-1, ac=4),
                                  equippable.armorTypes["Hands"],
                                  spawner=spawner)

# Legs
# ===================================
class LegsCloth(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Cloth Pants",
                                  equippable.Requirements(str=1),
                                  equippable.Stats(ac=1),
                                  equippable.armorTypes["Legs"],
                                  spawner=spawner)


class LegsLeather(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Leather Pantaloons",
                                  equippable.Requirements(str=4),
                                  equippable.Stats(ac=2),
                                  equippable.armorTypes["Legs"],
                                  spawner=spawner)


class LegsChain(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Chain Leggings",
                                  equippable.Requirements(str=9),
                                  equippable.Stats(dex=-1, ac=4),
                                  equippable.armorTypes["Legs"],
                                  spawner=spawner)


class LegsPlate(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Plate Sabatons",
                                  equippable.Requirements(str=17),
                                  equippable.Stats(dex=-2, ac=6),
                                  equippable.armorTypes["Legs"],
                                  spawner=spawner)

# Feet
# ===================================
class FeetCloth(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Cloth Shoes",
                                  equippable.Requirements(str=1),
                                  equippable.Stats(ac=1),
                                  equippable.armorTypes["Feet"],
                                  spawner=spawner)


class FeetLeather(equippable.Armor):
    def __init__(self, emap, spawner=False):
        equippable.Armor.__init__(self, libtcod.amber, emap,
                                  "no desc",
                                  "Leather Boots",
                                  equippable.Requirements(str=2),
                                  equippable.Stats(ac=2),
                                  equippable.armorTypes["Feet"],
                                  spawner=spawner)