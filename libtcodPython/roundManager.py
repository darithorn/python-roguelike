__author__ = 'darithorn'

import libtcodpy as libtcod
import events, entity, ui, spawner, player
import utility, loader


class RoundManager():
    def __init__(self, roundMap, player, round=0):
        global rm
        print 'New Round Manager!'
        self.roundMap = roundMap
        self.mobSpawner = spawner.MonsterSpawner(self.roundMap)
        self.itemSpawner = spawner.ItemSpawner(self.roundMap)
        self.tempMap = None
        self.player = player
        self.player.roundManager = self
        self.round = round
        self.exit = False
        self.reset = False
        utility.rm = self
        while events.running:
            if self.exit: break
            if self.reset: return
            self.next()
            self.mobSpawner.update()
        if self.exit:
            RoundManager(self.tempMap, self.player, self.round)

    def restart(self):
        spawner.mobList = {"1_1": None, "1_2": None,
                           "2_1": None, "2_2": None,
                           "3_1": None, "3_2": None,
                           "4_1": None, "4_2": None,
                           "5_1": None}
        spawner.itemList = {"1_1": None, "1_2": None,
                            "2_1": None, "2_2": None,
                            "3_1": None, "3_2": None,
                            "4_1": None, "4_2": None,
                            "5_1": None}
        ui.reset()
        m = loader.loadMap(1, 1)
        p = player.Player(m)
        ui.draw()
        utility.rm.reset = True
        RoundManager(m, p)

    def setMap(self, emap):
        self.exit = True
        self.tempMap = emap

    def next(self):
        self.round += 1
        ui.numRound = self.round
        for m in self.roundMap.mobs:
            if m.type == entity.entityTypes["Empty"]: continue
            m.go()
            while m.ableToMove:
                libtcod.console_clear(0)
                self.roundMap.draw()
                ui.draw()
                libtcod.console_flush()
                events.update()
                if not events.running or self.exit: return
                m.update()