__author__ = 'darithorn'

import libtcodpy as libtcod
import equippable

#Ammunition
class Arrow(equippable.Ammunition):
    def __init__(self, emap, amount=1, spawner=False):
        equippable.Ammunition.__init__(self, libtcod.peach, emap,
                                       "no desc",
                                       "Arrow",
                                       equippable.Requirements(),
                                       equippable.Stats(minDmg=1, maxDmg=3),
                                       equippable.ammunitionTypes["Arrow"],
                                       amount=amount,
                                       pluralName="Arrows",
                                       spawner=spawner)
        self.singularName = "Arrow"
        self.pluralName = "Arrows"
        self.stack(amount)
        self.setLevel(self.level)


class Bolt(equippable.Ammunition):
    def __init__(self, emap, amount=1, spawner=False):
        equippable.Ammunition.__init__(self, libtcod.peach, emap,
                                       "no desc",
                                       "Bolt",
                                       equippable.Requirements(),
                                       equippable.Stats(minDmg=2, maxDmg=4),
                                       equippable.ammunitionTypes["Bolt"],
                                       amount=amount,
                                       pluralName="Bolts",
                                       spawner=spawner)
        self.singularName = "Bolt"
        self.pluralName = "Bolts"
        self.stack(amount)
        self.setLevel(self.level)

# Swords
class RustSword(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.brass, emap,
                                   "A sword long passed its hayday. You wonder how it's lasted like this.",
                                   "Rusty Sword",
                                   equippable.Requirements(stam=2),
                                   equippable.Stats(minDmg=1, maxDmg=3),
                                   spawner=spawner)


class Broadsword(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "A medium sized sword with a broad blade",
                                   "Broadsword",
                                   equippable.Requirements(str=2, stam=3),
                                   equippable.Stats(minDmg=5, maxDmg=9),
                                   spawner=spawner)


class Claymore(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "A large and heavy sword used with two hands.",
                                   "Claymore",
                                   equippable.Requirements(str=4, stam=6, twoH=True),
                                   equippable.Stats(minDmg=9, maxDmg=13),
                                   spawner=spawner)

# Axes
class BattleAxe(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "no desc",
                                   "Battle Axe",
                                   equippable.Requirements(str=3, stam=4),
                                   equippable.Stats(minDmg=3, maxDmg=12),
                                   spawner=spawner)


class HaftedAxe(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "no desc",
                                   "Hafted Axe",
                                   equippable.Requirements(str=5, stam=8, twoH=True),
                                   equippable.Stats(minDmg=8, maxDmg=18),
                                   spawner=spawner)

# Blunt
class Morningstar(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "no desc",
                                   "Morning Star",
                                   equippable.Requirements(str=3, stam=5),
                                   equippable.Stats(minDmg=7, maxDmg=10),
                                   spawner=spawner)


class Maul(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "no desc",
                                   "Maul",
                                   equippable.Requirements(str=4, stam=7, twoH=True),
                                   equippable.Stats(minDmg=10, maxDmg=15),
                                   spawner=spawner)

# Polearms
class Spear(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "no desc",
                                   "Spear",
                                   equippable.Requirements(str=3, stam=4),
                                   equippable.Stats(minDmg=4, maxDmg=9),
                                   spawner=spawner)


class Voulge(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "no desc",
                                   "Voulge",
                                   equippable.Requirements(str=4, stam=7, twoH=True),
                                   equippable.Stats(minDmg=9, maxDmg=14),
                                   spawner=spawner)

#Ranged
class Longbow(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "no desc",
                                   "Longbow",
                                   equippable.Requirements(str=1, stam=3, twoH=True,
                                                           amm=equippable.ammunitionTypes["Arrow"]),
                                   equippable.Stats(minDmg=3, maxDmg=6),
                                   equippable.weaponTypes["Ranged"],
                                   spawner=spawner)


class Crossbow(equippable.Weapon):
    def __init__(self, emap, spawner=False):
        equippable.Weapon.__init__(self, libtcod.celadon, emap,
                                   "no desc",
                                   "Crossbow",
                                   equippable.Requirements(str=2, stam=7, twoH=True,
                                                           amm=equippable.ammunitionTypes["Bolt"]),
                                   equippable.Stats(minDmg=7, maxDmg=12),
                                   equippable.weaponTypes["Ranged"],
                                   spawner=spawner)