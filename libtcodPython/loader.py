__author__ = 'darithorn'

import libtcodpy as libtcod
import entity, vec2, tile, map

mapsLocation = 'resources/maps/'
rgbTiles = []


def append(rgbTile):
    rgbTiles.append(rgbTile)


class RGBTile():
    def __init__(self, r, g, b, t):
        """

        :param r: red value
        :param g: green value
        :param b: blue value
        :param t: tile
        """
        self.r = r
        self.g = g
        self.b = b
        self.t = t

    def equals(self, pix):
        return self.r == pix.r and self.g == pix.g and self.b == pix.b

    def tile(self, pos, emap):
        return self.t.new(pos, emap)


def loadMap(levelType, level):
    mapName = str(levelType) + '_' + str(level) + '.png'
    img = libtcod.image_load(mapsLocation + mapName)
    w, h = libtcod.image_get_size(img)
    m = map.Map(w, h, levelType, level)
    for y in range(h):
        for x in range(w):
            m.setTile(tile.Unknown().new(vec2.Vec2(x, y), m))
            pix = libtcod.image_get_pixel(img, x, y)
            for t in rgbTiles:
                if t.equals(pix):
                    m.setTile(t.tile(vec2.Vec2(x, y), m))
    m.findStart()
    m.findEnd()
    return m


def initTiles():
    append(RGBTile(0, 0, 0, tile.GenericWall()))
    append(RGBTile(170, 170, 170, tile.GenericFloor()))

    append(RGBTile(0, 200, 200, tile.PreviousLevel()))
    append(RGBTile(0, 200, 0, tile.NextLevel()))
    append(RGBTile(200, 0, 0, tile.DownStairs()))
    append(RGBTile(200, 0, 200, tile.UpStairs()))

    #Crypt
    append(RGBTile(0, 0, 200, tile.Starting()))
    append(RGBTile(255, 190, 255, tile.NoSpawnZoneCatacombs()))
    append(RGBTile(0, 170, 170, tile.StatueCatacombs()))
    append(RGBTile(0, 130, 130, tile.BrokenStatueCatacombs()))
    append(RGBTile(190, 160, 0, tile.Sarcophagus()))
    append(RGBTile(140, 120, 0, tile.BrokenSarcophagus()))
    append(RGBTile(120, 120, 120, tile.StonePillar()))
    append(RGBTile(100, 100, 100, tile.BrokenStonePillar()))
    append(RGBTile(230, 160, 160, tile.RubbleCatacombs()))
    append(RGBTile(0, 140, 0, tile.StoneTable()))
    append(RGBTile(160, 0, 0, tile.RippedCarpet()))
    append(RGBTile(160, 100, 0, tile.DirtWall()))
    append(RGBTile(200, 125, 0, tile.DirtFloor()))
    append(RGBTile(100, 90, 50, tile.BrokenCrate()))
    append(RGBTile(140, 130, 70, tile.WoodenCrate()))
    append(RGBTile(170, 150, 150, tile.CollapsedCeiling()))
    append(RGBTile(0, 100, 140, tile.CryptAltar()))
    append(RGBTile(0, 100, 0, tile.StoneChair()))
    append(RGBTile(200, 200, 0, tile.WoodenSupport()))

    # Orc Caves
    append(RGBTile(200, 80, 0, tile.OrcBanner()))
    append(RGBTile(195, 195, 195, tile.SkeletalRemains()))
    append(RGBTile(200, 80, 60, tile.OrcFeces()))
    append(RGBTile(200, 80, 120, tile.OrcBed()))
    append(RGBTile(200, 80, 200, tile.OrcTotem()))

    # Sewers
    append(RGBTile(0, 85, 35, tile.SewerWall()))
    append(RGBTile(0, 125, 35, tile.SewerFloor()))
    append(RGBTile(0, 95, 100, tile.SewerWater()))
    append(RGBTile(150, 250, 255, tile.WoodPlatform()))
    append(RGBTile(50, 85, 50, tile.Tent()))
    append(RGBTile(95, 85, 50, tile.MattedCarpet()))
    append(RGBTile(50, 150, 50, tile.StrangeMachine()))
    append(RGBTile(0, 150, 100, tile.DecayingFlesh()))

    # Forgotten City
    append(RGBTile(135, 135, 135, tile.Street()))
    append(RGBTile(170, 40, 40, tile.Carpet()))
    append(RGBTile(40, 170, 170, tile.WindowHorizontal()))
    append(RGBTile(50, 215, 215, tile.WindowVertical()))
    append(RGBTile(60, 100, 0, tile.Portcullis()))
    append(RGBTile(100, 100, 0, tile.Sign()))
    append(RGBTile(170, 40, 130, tile.Forge()))
    append(RGBTile(140, 75, 120, tile.Anvil()))
    append(RGBTile(25, 75, 120, tile.MarketStand()))
    append(RGBTile(100, 120, 200, tile.Loom()))
    append(RGBTile(25, 25, 25, tile.CollapsedWall()))
    append(RGBTile(25, 100, 25, tile.Debris()))
    append(RGBTile(20, 85, 85, tile.BrokenWindow()))
    append(RGBTile(200, 150, 120, tile.Bed()))
    append(RGBTile(150, 140, 0, tile.Table()))
    append(RGBTile(150, 100, 0, tile.Chair()))

    # Treasury
    append(RGBTile(255, 255, 0, tile.MeltedGold()))
    append(RGBTile(170, 170, 0, tile.CharredFloor()))
    append(RGBTile(110, 170, 0, tile.CharredWall()))

initTiles()