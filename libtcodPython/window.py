__author__ = 'darithorn'

import libtcodpy as libtcod
import object


class Option():
    def __init__(self, text, color, t, f=libtcod.KEY_ESCAPE):
        self.text = text
        self.color = color
        self.t = t
        self.f = f


class Console():
    def __init__(self, pos, size, options, name=""):
        self.pos = pos
        self.size = size
        self.options = options
        self.name = name

        self.console = libtcod.console_new(size.x - 2, size.y - 2)

    def draw(self):
        libtcod.console_clear(self.console)
        libtcod.console_set_default_foreground(0, libtcod.gray)
        libtcod.console_print_frame(0, self.pos.x, self.pos.y, self.size.x, self.size.y)
        libtcod.console_set_default_foreground(0, libtcod.lightest_gray)
        libtcod.console_print(0, self.pos.x + (self.size.x / 2) - (len(self.name) / 2), self.pos.y, self.name)
        count = 0
        for o in self.options:
            libtcod.console_set_default_foreground(self.console, o.color)
            if chr(o.t) != " ":
                libtcod.console_print(self.console, 1, 1 + count, chr(o.t) + " - " + o.text)
            else:
                libtcod.console_print(self.console, 1, 1 + count, chr(o.t) + o.text)
            count += 1
        libtcod.console_blit(self.console, 0, 0, 0, 0, 0, self.pos.x + 1, self.pos.y + 1)
        libtcod.console_flush()

    def wait(self):
        self.draw() # make sure the console drawn
        while True:
            key = libtcod.console_check_for_keypress()
            if key.vk == libtcod.KEY_ESCAPE:
                return False, -1
            count = 0
            for o in self.options:
                if key.c == o.t:
                    return True, count
                if key.c == o.f:
                    return False, -1
                count += 1

    def setTitle(self, title):
        self.title = title


class ListConsole(Console):
    def __init_(self, pos, size, li, name=""):
        options = self.createLetterList(li)
        Console.__init__(self, pos, size, options, name)

    def createLetterList(self, li):
        """

        :param li: list of Object
        :raise: li.count must be less than or equal to 24
        """
        if len(li) > 24:
            raise Exception("That list has more than 24 elements!")
        options = []
        count = 97
        for e in li:
            name = ""
            color = libtcod.lightest_gray
            if e.stackable:
                name += str(e.amount) + " "
            name += e.name + " "
            if e.objectType == object.objectTypes["Equippable"]:
                if e.equipped:
                    name += "E"
                    color = libtcod.green
            i = 0
            while i < self.size.x:
                name += " "
                i += 1
            options.append(Option(name, color, count))
            count += 1
        if len(li) < self.size.y:
            num = len(li) - 1
            t = ""
            i = 0
            while i < self.size.x:
                t += " "
                i += 1
            while num < self.size.y:
                options.append(Option(t, libtcod.green, ord(" ")))
                num += 1
        return options

    def setList(self, li):
        self.options = self.createLetterList(li)


class Column():
    def __init__(self, text, x, y, color):
        self.text = text
        self.x = x
        self.y = y
        self.color = color

class HelpConsole():
    def __init__(self, pos, size, name="Help"):
        self.size = size
        self.pos = pos
        self.name = name
        self.columns = []
        self.console = libtcod.console_new(size.x - 2, size.y - 2)

    def draw(self):
        libtcod.console_clear(self.console)
        libtcod.console_set_default_foreground(0, libtcod.gray)
        libtcod.console_print_frame(0, self.pos.x, self.pos.y, self.size.x, self.size.y)
        libtcod.console_set_default_foreground(0, libtcod.white)
        libtcod.console_print(0, self.pos.x + (self.size.x / 2) - (len(self.name) / 2), self.pos.y, self.name)
        for c in self.columns:
            libtcod.console_set_default_foreground(self.console, c.color)
            libtcod.console_print(self.console, c.x, c.y, c.text)
        libtcod.console_blit(self.console, 0, 0, 0, 0, 0, self.pos.x + 1, self.pos.y + 1)
        libtcod.console_flush()

    def getyPos(self, text):
        for c in self.columns:
            if c.text == text:
                return c.y
        return None

    def setTitle(self, n):
        self.name = n

    def appendln(self, line, x=1, y=1, cont=True, color=libtcod.lightest_gray):
        if cont and len(self.columns) > 0:
            y = self.columns[len(self.columns) - 1].y + 1
        self.columns.append(Column(line, x, y, color))

    def reset(self):
        self.text = []
        self.columns = []

class WonGameWindow(HelpConsole):
    def __init__(self):
        import vec2, prompt, events, utility
        HelpConsole.__init__(self, vec2.Vec2(0, 0), vec2.Vec2(90, 50), "")
        endMsg = "You killed the dragon! Congratulations!"
        self.appendln(endMsg, (self.size.x / 2) - (len(endMsg) / 2), (self.size.y / 2), False, libtcod.green)
        msg2 = "Press y to restart or n/ESC to quit!"
        self.appendln(msg2, (self.size.x / 2) - (len(msg2) / 2), color=libtcod.green)
        libtcod.console_clear(0)
        self.draw()
        answer = prompt.wait("NOTIHNG FOO", won=True)
        if answer == -1 or not answer:
            events.exitGame()
        else:
            utility.restart()