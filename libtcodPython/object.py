__author__ = 'darithorn'

import entity
import libtcodpy as libtcod

objectTypes = {"Unknown": 0, "Equippable": 1, "Object": 2}


class Object(entity.Entity):
    def __init__(self, pos, char, color, objectType, emap, desc, name,
                 backgroundColor=libtcod.black, stackable=False, amount=1, pluralName=" ", spawner=False):
        entity.Entity.__init__(self, pos, char, color, entity.entityTypes["Object"], emap, desc, False, False,
                               backgroundColor)
        self.spawner = spawner
        self.name = name
        self.singularName = self.name
        self.pluralName = pluralName
        self.objectType = objectType
        self.stackable = stackable
        self.space = 1
        self.pickupable = True
        self.amount = 0

        self.emap.addObj(self)

    def stack(self, amount):
        self.amount += amount
        if self.amount > 1:
            self.name = self.pluralName
        else:
            self.name = self.singularName

    def take(self, amount):
        self.amount -= amount
        if self.amount < 2:
            self.name = self.singularName

class Corpse(Object):
    def __init__(self, pos, color, emap):
        Object.__init__(self, pos, '%', color, objectTypes["Object"], emap, "no desc", "corpse")
        self.pickupable = False