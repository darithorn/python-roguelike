__author__ = 'darithorn'

import libtcodpy as libtcod
import player, roundManager, loader

def main():
    libtcod.console_set_custom_font("resources/terminal.png", libtcod.FONT_LAYOUT_ASCII_INCOL)
    libtcod.console_init_root(90, 50, "darithorn's 7DRL", False)
    libtcod.sys_set_fps(120)

    m = loader.loadMap(1, 1)
    p = player.Player(m)
    roundManager.RoundManager(m, p)


if __name__ == "__main__":
    main()
