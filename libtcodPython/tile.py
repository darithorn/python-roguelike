__author__ = 'darithorn'

import libtcodpy as libtcod
import entity, vec2, ui
import random

tileTypes = {"Unknown": 0, "Starting": 1, "NoSpawn": 2, "Lookable": 3, "Static": 4, "Up": 5, "Down": 6,
             "SpecialCarpet": 7}


# Set description to follow: You see (tile.desc).
class Tile(entity.Entity):
    def __init__(self, emap, pos, char, color, tileType, blocking, blockSight, desc, backgroundColor=libtcod.black):
        entity.Entity.__init__(self, pos, char, color, entity.entityTypes["Tile"], emap, desc, blocking, blockSight,
                               backgroundColor)
        self.tileType = tileType

    def use(self):
        ui.appendln("You can't do anything with this.")

    def steppedOn(self):
        pass

    def bump(self):
        ui.appendln("Ow!")

    def new(self, pos, emap):
        t = Tile(emap, pos, self.char, self.color, self.tileType, self.blocking, self.blockSight, self.desc,
                 self.backgroundColor)
        return t

class Unknown(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '?', libtcod.fuchsia, tileTypes["Unknown"], False, False,
                      "UNKNOWN")

class PreviousLevel(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), 'P', libtcod.azure, tileTypes["Up"], False, False,
                      "a path out of here")

    def use(self):
        import loader
        lt = self.emap.levelType
        l = self.emap.level
        if l == 1:
            l = 2
            lt -= 1
        else:
            l = 1
        p = self.emap.fovPlayer
        self.emap = loader.loadMap(lt, l)
        p.setEmapP(self.emap)
        ui.appendln("You walk to the previous level.")
        return True

    def new(self, pos, emap):
        t = PreviousLevel(emap)
        t.pos = pos
        return t

class NextLevel(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), 'N', libtcod.azure, tileTypes["Down"], False, False,
                      "a path out of here")

    def use(self):
        import loader
        lt = self.emap.levelType
        l = self.emap.level
        if l == 2:
            l = 1
            lt += 1
        else:
            l = 2
        p = self.emap.fovPlayer
        self.emap = loader.loadMap(lt, l)
        p.setEmapN(self.emap)
        ui.appendln("You walk to the next level.")
        return True

    def new(self, pos, emap):
        t = NextLevel(emap)
        t.pos = pos
        return t

class DownStairs(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '<', libtcod.dark_gray, tileTypes["Down"], False, False,
                      "stairs leading down")

    def use(self):
        import loader
        lt = self.emap.levelType
        l = self.emap.level
        if l == 2:
            l = 1
            lt += 1
        else:
            l = 2
        p = self.emap.fovPlayer
        self.emap = loader.loadMap(lt, l)
        p.setEmapN(self.emap)
        ui.appendln("You walk down the stairs")
        return True

    def new(self, pos, emap):
        t = DownStairs(emap)
        t.pos = pos
        return t

class UpStairs(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '<', libtcod.dark_gray, tileTypes["Up"], False, False,
                      "stairs leading up")

    def use(self):
        import loader
        lt = self.emap.levelType
        l = self.emap.level
        if l == 1:
            l = 2
            lt -= 1
        else:
            l = 1
        p = self.emap.fovPlayer
        self.emap = loader.loadMap(lt, l)
        p.setEmapP(self.emap)
        ui.appendln("You walk up the stairs.")
        return True

    def new(self, pos, emap):
        t = UpStairs(emap)
        t.pos = pos
        return t

class GenericWall(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '#', libtcod.dark_gray, tileTypes["Static"], True, True, "a stone wall")


class GenericFloor(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '+', libtcod.gray, tileTypes["Static"], False, False, "some flooring")


class Starting(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '>', libtcod.dark_gray, tileTypes["Starting"], False, False,
                      "a way out of here")


class NoSpawnZoneCatacombs(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '+', libtcod.gray, tileTypes["NoSpawn"], False, False, "some flooring")


class StatueCatacombs(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(10), libtcod.silver, tileTypes["Lookable"], True, True, "a statue")


class BrokenStatueCatacombs(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(9), libtcod.silver, tileTypes["Lookable"], True, False,
                      "a broken statue")


class Sarcophagus(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(155), libtcod.lightest_gray, tileTypes["Static"], True, False,
                      "a sarcophagus")


class BrokenSarcophagus(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(145), libtcod.lightest_gray, tileTypes["Static"], True, False,
                      "a broken sarcophagus")


class StonePillar(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), 'I', libtcod.darker_gray, tileTypes["Static"], True, True,
                      "a stone pillar")


class BrokenStonePillar(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(161), libtcod.darker_gray, tileTypes["Static"], True, False,
                      "a broken stone pillar")


class RubbleCatacombs(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(146), libtcod.sepia, tileTypes["Static"], True, True,
                      "some rubble")

class StoneTable(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(194), libtcod.lightest_gray, tileTypes["Static"], True, False,
                      "a stone table")

class RippedCarpet(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '+', libtcod.darker_crimson, tileTypes["Static"], False, False,
                      "some ripped carpet")

class DirtWall(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '#', libtcod.dark_sepia, tileTypes["Static"], True, True,
                      "a dirt wall")

class DirtFloor(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), ',', libtcod.sepia, tileTypes["Static"], False, False,
                      "some dirt")

    def new(self, pos, emap):
        num = random.randint(0, 100)
        char = self.char
        # , . ' "
        if num <= 25:
            char = ','
        elif 25 < num <= 50:
            char = '.'
        elif 50 < num <= 75:
            char = '\''
        elif 75 < num <= 100:
            char = '\"'
        t = Tile(emap, pos, char, self.color, self.tileType, self.blocking, self.blockSight, self.desc,
                 self.backgroundColor)
        return t

class BrokenCrate(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(19), libtcod.darker_amber, tileTypes["Static"], True, False,
                      "a broken crate")

class WoodenCrate(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(224), libtcod.darker_amber, tileTypes["Static"], True, False,
                      "a crate")

class CollapsedCeiling(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(127), libtcod.desaturated_flame, tileTypes["Static"], True, True,
                      "a collapsed ceiling")

class CryptAltar(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(15), libtcod.sky, tileTypes["Static"], True, False,
                      "an altar")

class StoneChair(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(195), libtcod.lightest_gray, tileTypes["Static"], False, False,
                      "a stone chair")

class WoodenSupport(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), 'I', libtcod.darker_orange, tileTypes["Static"], True, False,
                      "a wooden support")

class OrcBanner(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(5), libtcod.flame, tileTypes["Static"], True, False,
                      "an orc banner")

class SkeletalRemains(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '%', libtcod.lightest_gray, tileTypes["Static"], False, False,
                      "some skeletal remains")

    def steppedOn(self):
        ui.appendln("Crunch!")

    def new(self, pos, emap):
        t = SkeletalRemains(emap)
        t.pos = pos
        return t


class OrcFeces(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(248), libtcod.dark_sepia, tileTypes["Static"], False, False,
                      "a pile of orc feces")

    def steppedOn(self):
        ui.appendln("Yuck!")

    def new(self, pos, emap):
        t = OrcFeces(emap)
        t.pos = pos
        return t

class OrcBed(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '0', libtcod.flame, tileTypes["Static"], True, False,
                      "an orc bed")

class OrcTotem(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(21), libtcod.flame, tileTypes["Static"], True, False,
                      "an orc totem")

class SewerWall(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '#', libtcod.darker_gray, tileTypes["Static"], True, True,
                      "a sewer wall")

class SewerFloor(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '+', libtcod.gray, tileTypes["Static"], False, False,
                      "some flooring")

class SewerWater(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '~', libtcod.dark_green, tileTypes["Static"], True, False,
                      "some sewer water")
        self.otherTile = chr(243)
        self.mainTile = self.char
        self.onOtherTile = False
        self.time = 200
        self.timer = random.randint(0, self.time)

    def bump(self):
        pass

    def update(self):
        self.timer += 1
        if self.timer >= self.time:
            self.timer = random.randint(0, self.time)
            if not self.onOtherTile:
                self.onOtherTile = True
                self.char = self.otherTile
            else:
                self.onOtherTile = False
                self.char = self.mainTile

    def draw(self):
        libtcod.console_set_default_background(0, self.backgroundColor)
        libtcod.console_set_default_foreground(0, self.color)
        libtcod.console_put_char(0, self.pos.x, self.pos.y, self.char, libtcod.BKGND_SET)

    def new(self, pos, emap):
        t = SewerWater(emap)
        t.pos = pos
        return t

class WoodPlatform(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '+', libtcod.light_sepia, tileTypes["Static"], False, False,
                      "a wood platform")

class Tent(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '#', libtcod.desaturated_sky, tileTypes["Static"], True, True,
                      "a tent wall")

class MattedCarpet(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '+', libtcod.desaturated_green, tileTypes["Static"], False, False,
                      "some matted carpet")

class StrangeMachine(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '&', libtcod.desaturated_lime, tileTypes["Static"], True, False,
                      "a strange machine")

class DecayingFlesh(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(239), libtcod.darker_green, tileTypes["Static"], False, False,
                      "some decaying flesh")

class Street(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '+', libtcod.darker_gray, tileTypes["Static"], False, False,
                      "a street")

class Carpet(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '+', libtcod.desaturated_red, tileTypes["Static"], False, False,
                      "a carpet")

class WindowHorizontal(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '_', libtcod.light_sky, tileTypes["Static"], True, False,
                      "a window")

class WindowVertical(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(179), libtcod.light_sky, tileTypes["Static"], True, False,
                      "a window")

class Portcullis(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(178), libtcod.lightest_gray, tileTypes["Static"], True, False,
                      "a portcullis")

class Sign(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(18), libtcod.light_sepia, tileTypes["Static"], True, False,
                      "a wooden sign")

class Forge(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(8), libtcod.flame, tileTypes["Static"], True, False,
                      "a forge")

class Anvil(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(7), libtcod.darkest_gray, tileTypes["Static"], True, False,
                      "an anvil")

class MarketStand(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(206), libtcod.darker_fuchsia, tileTypes["Static"], True, False,
                      "a market stand")

class Loom(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(14), libtcod.sepia, tileTypes["Static"], True, False,
                      "a loom")

class CollapsedWall(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(29), libtcod.gray, tileTypes["Static"], True, True,
                      "a collapsed wall")

class Debris(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '%', libtcod.dark_sepia, tileTypes["Static"], False, False,
                      "some debris")

class BrokenWindow(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), ':', libtcod.light_sky, tileTypes["Static"], False, False,
                      "a broken window")

class Bed(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '0', libtcod.light_sepia, tileTypes["Static"], True, False,
                      "a bed")

class Table(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(194), libtcod.darkest_sepia, tileTypes["Static"], True, False,
                      "a table")

class Chair(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(195), libtcod.sepia, tileTypes["Static"], False, False,
                      "a chair")

class MeltedGold(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), chr(156), libtcod.gold, tileTypes["Static"], False, False,
                      "some melted gold")

class CharredFloor(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '+', libtcod.darker_gray, tileTypes["Static"], False, False,
                      "a charred floor")

class CharredWall(Tile):
    def __init__(self, emap=0):
        Tile.__init__(self, emap, vec2.Vec2(0, 0), '#', libtcod.darkest_gray, tileTypes["Static"], True, True,
                      "a charred wall")