__author__ = 'darithorn'

import libtcodpy as libtcod
import monsters, vec2
import armor, weapons, shields, equippable
import random


class Chance():
    def __init__(self, min, max):
        self.min = min
        self.max = max

    def random(self):
        return self.min < random.randint(0, 100) < self.max


mobList = {"1_1": None, "1_2": None,
           "2_1": None, "2_2": None,
           "3_1": None, "3_2": None,
           "4_1": None, "4_2": None,
           "5_1": None}
itemList = {"1_1": None, "1_2": None,
            "2_1": None, "2_2": None,
            "3_1": None, "3_2": None,
            "4_1": None, "4_2": None,
            "5_1": None}


class MonsterSpawner():
    def __init__(self, emap):
        self.emap = emap
        self.mapLevel = str(self.emap.levelType) + "_" + str(self.emap.level)

        # Create lists weakest -> strongest
        self._1_1chances = [Chance(0, 80), Chance(0, 30), Chance(0, 10)]
        self._1_1 = [monsters.Skeleton(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Spectre(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Zombie(vec2.Vec2(0, 0), emap, spawner=True)]

        self._1_2chances = [Chance(0, 70), Chance(0, 40), Chance(0, 25), Chance(0, 10)]
        self._1_2 = [monsters.Spectre(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Zombie(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Mummy(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Lich(vec2.Vec2(0, 0), emap, spawner=True)]

        self._2_1chances = [Chance(0, 90), Chance(0, 30), Chance(0, 10)]
        self._2_1 = [monsters.Goblin(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.OrcWarrior(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.GoblinShaman(vec2.Vec2(0, 0), emap, spawner=True)]

        self._2_2chances = [Chance(0, 60), Chance(0, 40), Chance(0, 10), Chance(0, 20)]
        self._2_2 = [monsters.OrcWarrior(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.GoblinShaman(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Troll(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.OrcWarlord(vec2.Vec2(0, 0), emap, spawner=True)]

        self._3_1chances = [Chance(0, 100), Chance(0, 30), Chance(0, 10)]
        self._3_1 = [monsters.RatmanRogue(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.RatmanSeer(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.RatmanEngineer(vec2.Vec2(0, 0), emap, spawner=True)]

        self._3_2chances = [Chance(0, 100), Chance(0, 50), Chance(0, 25), Chance(0, 20)]
        self._3_2 = [monsters.RatmanSeer(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.RatmanEngineer(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Beastman(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.RatOgre(vec2.Vec2(0, 0), emap, spawner=True)]

        self._4_1chances = [Chance(0, 100), Chance(0, 40), Chance(0, 15)]
        self._4_1 = [monsters.TaintedDwarf(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Succubus(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Drow(vec2.Vec2(0, 0), emap, spawner=True)]

        self._4_2chances = [Chance(0, 100), Chance(0, 30), Chance(0, 20), Chance(0, 5)]
        self._4_2 = [monsters.Succubus(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Drow(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.ForgottenOne(vec2.Vec2(0, 0), emap, spawner=True),
                     monsters.Demon(vec2.Vec2(0, 0), emap, spawner=True)]

        self._5_1chances = [Chance(0, 100)]
        self._5_1 = [monsters.Dragon(vec2.Vec2(0, 0), emap, spawner=True)]

        self.mobsToSpawn = {"1_1": self._1_1, "1_2": self._1_2,
                            "2_1": self._2_1, "2_2": self._2_2,
                            "3_1": self._3_1, "3_2": self._3_2,
                            "4_1": self._4_1, "4_2": self._4_2,
                            "5_1": self._5_1}
        self.mobsChances = {"1_1": self._1_1chances, "1_2": self._1_2chances,
                            "2_1": self._2_1chances, "2_2": self._2_2chances,
                            "3_1": self._3_1chances, "3_2": self._3_2chances,
                            "4_1": self._4_1chances, "4_2": self._4_2chances,
                            "5_1": self._5_1chances}
        self.usedMobList = self.mobsToSpawn[self.mapLevel]
        self.usedChances = self.mobsChances[self.mapLevel]
        self.spawnTimer = 0
        self.totalSpawnTimer = 30
        self.timeIncrease = 1.05
        self.monsterAmount = 0
        self.maxMonsterAmount = 20

        if mobList[self.mapLevel] is not None:
            self.emap.mobs += mobList[self.mapLevel]
        else:
            self.populateLevel()
            mobList[self.mapLevel] = self.emap.mobs

    def populateLevel(self):
        print 'Populated level: ' + self.mapLevel
        while self.monsterAmount < self.maxMonsterAmount:
            self.spawn()

    def spawn(self):
        i = 0
        mobToSpawn = None
        for c in self.usedChances:
            if c.random():
                mobToSpawn = self.usedMobList[i]
            i += 1
        if mobToSpawn is None: mobToSpawn = self.usedMobList[0]
        pos = vec2.Vec2(0, 0)
        while self.emap.getTile(pos).blocking:
            pos.x = random.randint(1, self.emap.width - 1)
            pos.y = random.randint(1, self.emap.height - 1)
        mobToSpawn.copy(pos, self.emap)
        self.monsterAmount += 1
        print 'SPAWNED: ', mobToSpawn.name, ' at ', '[', pos.x, ',', pos.y, ']'

    def update(self):
        if self.monsterAmount < self.maxMonsterAmount:
            self.spawnTimer += 1
            if self.spawnTimer >= self.totalSpawnTimer:
                self.spawnTimer = 0
                self.totalSpawnTimer *= self.timeIncrease
                self.spawn()


class ItemSpawner():
    def __init__(self, emap):
        self.emap = emap
        self.mapLevel = str(self.emap.levelType) + "_" + str(self.emap.level)

        self.clothArmor = [armor.FeetCloth(emap, spawner=True), armor.HandsCloth(emap, spawner=True),
                           armor.TorsoCloth(emap, spawner=True), armor.LegsCloth(emap, spawner=True),
                           armor.HeadCloth(emap, spawner=True)]

        self.leatherArmor = [armor.FeetLeather(emap, spawner=True), armor.HandsLeather(emap, spawner=True),
                             armor.TorsoLeather(emap, spawner=True),
                             armor.LegsLeather(emap, spawner=True), armor.HeadLeather(emap, spawner=True)]

        self.chainArmor = [armor.HandsChain(emap, spawner=True), armor.TorsoChain(emap, spawner=True),
                           armor.LegsChain(emap, spawner=True), armor.HeadChain(emap, spawner=True)]

        self.plateArmor = [armor.HandsPlate(emap, spawner=True), armor.TorsoPlate(emap, spawner=True),
                           armor.LegsPlate(emap, spawner=True), armor.HeadPlate(emap, spawner=True)]

        self.weaponList = [weapons.Broadsword(emap, spawner=True), weapons.Claymore(emap, spawner=True),
                           weapons.BattleAxe(emap, spawner=True), weapons.HaftedAxe(emap, spawner=True),
                           weapons.Morningstar(emap, spawner=True), weapons.Maul(emap, spawner=True),
                           weapons.Spear(emap, spawner=True), weapons.Voulge(emap, spawner=True),
                           weapons.Longbow(emap, spawner=True), weapons.Crossbow(emap, spawner=True)]

        self.ammoList = [weapons.Arrow(emap, spawner=True), weapons.Bolt(emap, spawner=True)]

        self.shieldList1 = [shields.Buckler(emap, spawner=True), shields.WoodenShield(emap, spawner=True)]
        self.shieldList2 = [shields.Buckler(emap, spawner=True), shields.WoodenShield(emap, spawner=True),
                            shields.Rondache(emap, spawner=True)]
        self.shieldList3 = [shields.Buckler(emap, spawner=True), shields.WoodenShield(emap, spawner=True),
                            shields.Rondache(emap, spawner=True),
                            shields.TearKiteShield(emap, spawner=True)]
        self.shieldList4 = [shields.Buckler(emap, spawner=True), shields.WoodenShield(emap, spawner=True),
                            shields.Rondache(emap, spawner=True),
                            shields.TearKiteShield(emap, spawner=True), shields.HeaterShield(emap, spawner=True)]

        self.shieldList = {"1_1": self.shieldList1, "1_2": self.shieldList1,
                           "2_1": self.shieldList2, "2_2": self.shieldList2,
                           "3_1": self.shieldList3, "3_2": self.shieldList3,
                           "4_1": self.shieldList4, "4_2": self.shieldList4,
                           "5_1": self.shieldList4}

        self.shieldListUsing = self.shieldList[self.mapLevel]

        self.armorSpawnList1 = [self.clothArmor, self.leatherArmor]
        self.armorSpawnList2 = self.armorSpawnList1 + [self.chainArmor]
        self.armorSpawnList3 = self.armorSpawnList2 + [self.plateArmor]

        self.armorList = {"1_1": self.armorSpawnList1, "1_2": self.armorSpawnList1,
                          "2_1": self.armorSpawnList2, "2_2": self.armorSpawnList2,
                          "3_1": self.armorSpawnList3, "3_2": self.armorSpawnList3,
                          "4_1": self.armorSpawnList3, "4_2": self.armorSpawnList3,
                          "5_1": self.armorSpawnList3}

        self.armorListUsing = self.armorList[self.mapLevel]

        self._1_1chances = [Chance(0, 100), Chance(0, 5)]
        self._1_2chances = [Chance(0, 100), Chance(0, 15)]
        self._2_1chances = [Chance(0, 100), Chance(0, 25), Chance(0, 5)]
        self._2_2chances = [Chance(0, 100), Chance(0, 40), Chance(0, 10)]
        self._3_1chances = [Chance(0, 100), Chance(0, 100), Chance(0, 20), Chance(0, 5)]
        self._3_2chances = [Chance(0, 100), Chance(0, 100), Chance(0, 30), Chance(0, 10)]
        self._4_1chances = [Chance(0, 100), Chance(0, 100), Chance(0, 40), Chance(0, 10), Chance(0, 5)]
        self._4_2chances = [Chance(0, 100), Chance(0, 100), Chance(0, 45), Chance(0, 15), Chance(0, 10)]
        self._5_1chances = [Chance(0, 100), Chance(0, 100), Chance(0, 50), Chance(0, 15), Chance(0, 10), Chance(0, 5)]
        self.levelChances = {"1_1": self._1_1chances, "1_2": self._1_2chances,
                             "2_1": self._2_1chances, "2_2": self._2_2chances,
                             "3_1": self._3_1chances, "3_2": self._3_2chances,
                             "4_1": self._4_1chances, "4_2": self._4_2chances,
                             "5_1": self._5_1chances}
        self.usedlevelChance = self.levelChances[self.mapLevel]

        #self.spawnTimer = 0
        #self.totalSpawnTimer = 100
        #self.timeIncrease = 1.05
        self.itemAmount = 0
        self.maxItemAmount = 15

        if itemList[self.mapLevel] is not None:
            self.emap.objects = itemList[self.mapLevel]
        else:
            self.populateLevel()
            itemList[self.mapLevel] = self.emap.objects

    def populateLevel(self):
        while self.itemAmount != self.maxItemAmount:
            self.spawn()

    def spawn(self):
        t = random.randint(1, 4)
        i = 1
        level = 1
        for c in self.usedlevelChance:
            if c.random():
                level = i
            i += 1
        itemToSpawn = None
        pos = vec2.Vec2(0, 0)
        while self.emap.getTile(pos).blocking:
            pos.x = random.randint(1, self.emap.width - 1)
            pos.y = random.randint(1, self.emap.height - 1)
        if t == 1: # armor
            i = self.armorListUsing[random.randint(0, len(self.armorListUsing) - 1)]
            itemToSpawn = i[random.randint(0, len(i) - 1)].copy(pos, self.emap)
        elif t == 2: # weapons
            itemToSpawn = self.weaponList[random.randint(0, len(self.weaponList) - 1)].copy(pos, self.emap)
        elif t == 3: # ammo
            amount = random.randint(1, 30)
            itemToSpawn = self.ammoList[random.randint(0, len(self.ammoList) - 1)].copy(pos, self.emap)
            itemToSpawn.stack(amount)
            itemToSpawn.realName = itemToSpawn.name
        elif t == 4: # shield
            itemToSpawn = self.shieldListUsing[random.randint(0, len(self.shieldListUsing) - 1)].copy(pos, self.emap)
        itemToSpawn.setLevel(level)
        print 'ITEM: ', itemToSpawn.name, ' at ', '[', pos.x, ',', pos.y, ']'
        self.itemAmount += 1



