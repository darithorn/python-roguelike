__author__ = 'darithorn'

import libtcodpy as libtcod

root = 0
blank = libtcod.console_new(30, 25)
log = libtcod.console_new(37, 23) # minus 3 from x for frame and +1 offset (51) - minus 2 from y for frame
stats = libtcod.console_new(37, 23)
logMessages = []
colors = []
newline = []
startCount = 0
player = None
numRound = 0

def reset(resetPlayer=True):
    global logMessages, colors, newline, startCount, player, numRound
    logMessages = []
    colors = []
    newline = []
    startCount = 0
    numRound = 1
    if resetPlayer:
        player = None

def remove(text):
    count = len(logMessages) - 1
    while count > 0:
        log = logMessages[count]
        if text == log:
            logMessages.pop(count)
            colors.pop(count)
            newline.pop(count)
            return
        count -= 1

def append(text, color=libtcod.lightest_gray, won=False):
    logMessages.append(text)
    colors.append(color)
    newline.append(False)
    if len(logMessages) > 23:
        global startCount
        startCount += 1
    if not won:
        draw()


def appendln(text, color=libtcod.lightest_gray):
    logMessages.append(text)
    colors.append(color)
    newline.append(True)
    if len(logMessages) > 23:
        global startCount
        startCount += 1
    draw()


def draw():
    libtcod.console_clear(log)
    libtcod.console_clear(stats)

    # Log UI
    libtcod.console_set_default_foreground(root, libtcod.gray)
    libtcod.console_set_default_background(root, libtcod.black)
    libtcod.console_print_frame(root, 50, 0, 40, 25)
    count = startCount
    y = 0
    x = 0
    while count < len(logMessages):
        libtcod.console_set_default_foreground(log, colors[count])
        libtcod.console_print(log, x, y, logMessages[count])
        x = len(logMessages[count])
        if newline[count]:
            y += 1
            x = 0
        count += 1

    libtcod.console_blit(log, 0, 0, 0, 0, root, 51, 1) # blit log

    # Player Stats
    libtcod.console_set_default_foreground(root, libtcod.gray)
    libtcod.console_set_default_background(root, libtcod.black)
    libtcod.console_print_frame(root, 50, 25, 40, 25)

    libtcod.console_set_default_foreground(stats, libtcod.gray)
    if player.speed != 1:
        libtcod.console_print(stats, 1, 1, "Move " + str(player.moves + 1) + " out of " + str(player.speed))
    else:
        libtcod.console_print(stats, 1, 1, "Moves: " + str(player.speed))

    libtcod.console_print(stats, 1, 2, "Round: " + str(numRound))

    libtcod.console_print(stats, 11, 8, "Level: " + str(player.level))
    libtcod.console_print(stats, 11, 9, "XP: " + str(player.exp) + "/" + str(player.maxexp))

    libtcod.console_print(stats, 1, 4, "HP: ")
    if player.hp / player.maxhp < 0.4:
        libtcod.console_set_default_foreground(stats, libtcod.red)
    elif 0.6 > player.hp / player.maxhp > 0.4:
        libtcod.console_set_default_foreground(stats, libtcod.yellow)
    else:
        libtcod.console_set_default_foreground(stats, libtcod.gray)
    libtcod.console_print(stats, 5, 4, str(int(player.hp)))
    libtcod.console_set_default_foreground(stats, libtcod.gray)
    libtcod.console_print(stats, 5 + len(str(int(player.hp))), 4, '/' + str(int(player.maxhp)))

    libtcod.console_print(stats, 1, 5, "SP: ")
    if player.stam / player.maxstam < 0.4:
        libtcod.console_set_default_foreground(stats, libtcod.red)
    elif 0.6 > player.stam / player.maxstam > 0.4:
        libtcod.console_set_default_foreground(stats, libtcod.yellow)
    else:
        libtcod.console_set_default_foreground(stats, libtcod.gray)
    libtcod.console_print(stats, 5, 5, str(int(player.stam)))
    libtcod.console_set_default_foreground(stats, libtcod.gray)
    libtcod.console_print(stats, 5 + len(str(int(player.stam))), 5, "/" + str(int(player.maxstam)))

    #libtcod.console_print(stats, 1, 6, "MP: " + str(int(player.mp)) + "/" + str(int(player.maxmp)))

    libtcod.console_print(stats, 1, 8, "STR: " + str(player.str))
    libtcod.console_print(stats, 1, 9, "DEX: " + str(player.dex))
    #libtcod.console_print(stats, 1, 10, "INT: " + str(player.int))
    #libtcod.console_print(stats, 1, 11, "WPR: " + str(player.wpr))
    libtcod.console_print(stats, 1, 10, "PER: " + str(player.perception))

    libtcod.console_print(stats, 1, 12, "DMG: " + str(player.mindmg) + " - " + str(player.maxdmg))

    libtcod.console_print(stats, 1, 14, "AC: " + str(player.armorClass))
    #libtcod.console_print(stats, 1, 17, "rP: " + str(player.poisonResist))
    #libtcod.console_print(stats, 1, 18, "rF: " + str(player.fireResist))
    #libtcod.console_print(stats, 1, 19, "rC: " + str(player.coldResist))
    #libtcod.console_print(stats, 1, 20, "rT: " + str(player.taintResist))

    libtcod.console_blit(stats, 0, 0, 0, 0, root, 51, 26) # blit stats
