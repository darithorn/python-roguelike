__author__ = 'darithorn'

import libtcodpy as libtcod
import ui, prompt

running = True
key = 0
modes = {"Inventory": 0, "Equipping": 1, "PickingUp": 2, "Dropping": 3, "Looking": 4, "Default": 5, "Examine": 6,
         "Prompt": 7, "Help": 8, "RestingUntilDone": 9, "RestingForTurns": 10, "Firing": 11}
mode = modes["Default"]


def exitGame():
    global running
    running = False
    turnModeOff()


def handleGlobalEvents(key):
    global running
    if key.vk == libtcod.KEY_PAGEUP:
        if ui.startCount > 0: ui.startCount -= 1

    if key.vk == libtcod.KEY_PAGEDOWN:
        if ui.startCount < len(ui.logMessages) - 23:
            ui.startCount += 1

    if key.vk == libtcod.KEY_ESCAPE:
        if mode == modes["Default"]:
            e = prompt.wait("Really exit? (y/n) ", libtcod.yellow)
            if e == -1:
                ui.appendln("Keep on playing!")
            elif e:
                running = False
            else:
                ui.appendln("Keep on playing!")
        turnModeOff()

    if libtcod.console_is_window_closed():
        exitGame()


def update():
    global key
    key = libtcod.console_check_for_keypress()
    handleGlobalEvents(key)


def turnModeOn(m):
    global mode
    mode = m


def turnModeOff():
    global mode
    mode = modes["Default"]
