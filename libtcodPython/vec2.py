__author__ = 'darithorn'

import math


class Vec2():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def add(self, dx, dy):
        self.x += dx
        self.y += dy

    def equals(self, pos):
        return pos.x == self.x and pos.y == self.y

    def nextTo(self, pos):
        dx = pos.x - self.x
        dy = pos.y - self.y
        if dx < 0: dx *= -1
        if dy < 0: dy *= -1
        ntx = False
        nty = False
        if dx == 1 or dx == 0:
            ntx = True
        if dy == 1 or dy == 0:
            nty = True
        return ntx and nty

    def distance(self, pos):
        return math.sqrt(math.pow(pos.x - self.x, 2) + math.pow(pos.y - self.y, 2))

    def copy(self):
        return Vec2(self.x, self.y)
