__author__ = 'darithorn'

import libtcodpy as libtcod
import tile, vec2


class Map():
    def __init__(self, width, height, levelType, level):
        self.width = width
        self.height = height
        self.levelType = levelType
        self.level = level

        self.startPos = vec2.Vec2(0, 0)
        self.endPos = vec2.Vec2(0, 0)

        self.tiles = [[0 for y in range(height)] for x in range(width)]
        self.objects = []
        self.mobs = []

        self.fovPlayer = None
        self.fov = libtcod.map_new(width, height)

    def findStart(self):
        if self.levelType == 1 and self.level == 1:
            found, t = self.findTile(tile.tileTypes["Starting"])
            if found:
                self.startPos = t.pos
        else:
            found, t = self.findTile(tile.tileTypes["Up"])
            if found:
                self.startPos = t.pos

    def findEnd(self):
        found, t = self.findTile(tile.tileTypes["Down"])
        if found:
            self.endPos = t.pos

    def findTile(self, type):
        for tl in self.tiles:
            for t in tl:
                if t.tileType == type:
                    return True, t
        return False, None

    def setProperty(self, e):
        libtcod.map_set_properties(self.fov, e.pos.x, e.pos.y, not e.blockSight, not e.blocking)

    def resetProperty(self, entity):
        self.setProperty(self.tiles[entity.pos.x][entity.pos.y])

    def setTile(self, tile):
        self.tiles[tile.pos.x][tile.pos.y] = tile
        self.setProperty(tile)

    def getTile(self, pos):
        return self.tiles[pos.x][pos.y]

    def getTiles(self, type):
        tiles = []
        for tl in self.tiles:
            for t in tl:
                if t == 0: continue
                if t.tileType == type:
                    tiles.append(t)
        return tiles

    def removeObj(self, obj):
        self.objects.remove(obj)

    def addObj(self, obj):
        self.objects.append(obj)

    def addMob(self, mob):
        self.mobs.append(mob)
        self.setProperty(mob)

    def removeMob(self, mob):
        self.resetProperty(mob)
        self.mobs.remove(mob)

    def removeMobByName(self, name):
        for m in self.mobs:
            if m.name == name:
                self.removeMob(m)
                return

    def getItems(self, pos):
        objs = []
        for o in self.objects:
            if o.pos.x == pos.x and o.pos.y == pos.y:
                objs.append(o)
        return objs

    def getMobs(self, pos):
        for m in self.mobs:
            if m.pos.x == pos.x and m.pos.y == pos.y:
                return m
        return None

    def draw(self, startx=0, starty=0, w=-1, h=-1):
        libtcod.map_compute_fov(self.fov, self.fovPlayer.pos.x, self.fovPlayer.pos.y, self.fovPlayer.perception,
                                libtcod.FOV_RESTRICTIVE)
        global width
        global height

        for tl in self.tiles:
            for t in tl:
                t.update()

        if w == -1:
            width = self.width
        else:
            width = w
        if h == -1:
            height = self.height
        else:
            height = h

        while starty < height:
            while startx < width:

                if self.tiles[startx][starty] != 0 and libtcod.map_is_in_fov(self.fov, startx, starty):
                    self.tiles[startx][starty].draw()

                #if libtcod.map_is_in_fov(self.fov, startx, starty):
                #    if libtcod.map_is_walkable(self.fov, startx, starty):
                #        libtcod.console_set_default_background(0, libtcod.desaturated_green)
                #        libtcod.console_put_char(0, startx, starty, ' ', libtcod.BKGND_SET)
                #    else:
                #        libtcod.console_set_default_background(0, libtcod.desaturated_red)
                #        libtcod.console_put_char(0, startx, starty, ' ', libtcod.BKGND_SET)

                startx += 1
            starty += 1
            startx = 0

        for o in self.objects:
            if libtcod.map_is_in_fov(self.fov, o.pos.x, o.pos.y) and not o.spawner:
                o.draw()

        for m in self.mobs:
            if libtcod.map_is_in_fov(self.fov, m.pos.x, m.pos.y) and not m.spawner:
                m.draw()