__author__ = 'darithorn'

import libtcodpy as libtcod
import mob, events, window, vec2, ui, utility, equippable, object
import weapons, armor, prompt
import random


class Player(mob.Mob):
    def __init__(self, emap):
        mob.Mob.__init__(self, vec2.Vec2(0, 0), '@', libtcod.blue, mob.mobTypes["Player"], emap, "", "you")
        self.setPos(self.emap.startPos.x, self.emap.startPos.y)
        self.emap.fovPlayer = self

        ui.player = self

        self.str = 1
        self.dex = 1
        self.int = 1
        self.wpr = 1

        self.restTurns = 0
        self.totalRestTurns = 0

        self.level = 0
        self.exp = 0
        self.maxexp = 10
        self.expIncrease = 1.2

        self.leveled()
        self.exp = 0
        self.maxexp = 10

        self.hp = self.maxhp
        self.stam = self.maxstam
        self.mp = self.maxmp

        self.speed = 1
        self.inventory = []
        self.inventorySpace = 0
        self.inventorySize = vec2.Vec2(30, 30)
        self.inventoryPos = vec2.Vec2((self.emap.width / 2) - (self.inventorySize.x / 2),
                                      (self.emap.height / 2) - (self.inventorySize.y / 2))
        self.inventoryWindow = window.ListConsole(self.inventoryPos, self.inventorySize, self.inventory, "Inventory")
        self.helpSize = vec2.Vec2(90, 50)
        self.helpPos = vec2.Vec2(0, 0)
        self.helpWindow = window.HelpConsole(self.helpPos, self.helpSize)
        self.initHelp()

        self.examineSize = vec2.Vec2(30, 30)
        self.examinePos = vec2.Vec2((self.emap.width / 2) - (self.examineSize.x / 2),
                                    (self.emap.height / 2) - (self.examineSize.y / 2))
        self.examineWindow = window.HelpConsole(self.examinePos, self.examineSize, "Examine")

        weaponChoices = [weapons.Broadsword(self.emap), weapons.BattleAxe(self.emap),
                         weapons.Morningstar(self.emap), weapons.Spear(self.emap), weapons.Longbow(self.emap)]

        ui.reset(False)

        ui.appendln("Select a weapon!", libtcod.azure)
        ui.appendln("1. Broadsword(str:2 stam:3 dmg:5-9)")
        ui.appendln("2. Battle Axe(str:3 stam:4 dmg:3-12)")
        ui.appendln("3. Morningstar(str:3 stam:5 dmg:7-10)")
        ui.appendln("4. Spear(str:3 stam:4 dmg:4-9)")
        ui.appendln("5. Longbow(str:1 stam:3 dmg:3-6)")

        num = -1
        while num == -1:
            num = prompt.waitNumber("Enter a number (1-5): ")
            if num < 1 or num > 5: num = -1

        self.startingItem(weaponChoices[num - 1])
        if num == 5:
            self.startingItem(weapons.Arrow(self.emap, amount=30))

        self.startingItem(armor.Rags(self.emap))

        ui.reset(False)

        ui.appendln("Use ? to access help!", libtcod.green)

        self.updateStats()
        if utility.testing:
            self.armorClass = 1000
            self.mindmg = 1000
            self.maxdmg = 1000
            self.hitChance = 1000
        self.hp = self.maxhp
        self.stam = self.maxstam
        self.mp = self.maxmp

    def startingItem(self, item, equip=True):
        self.pickupItem(item)
        if equip:
            self.equip(item)

    def setEmapP(self, emap):
        self.emap = emap
        self.emap.addMob(self)
        self.emap.fovPlayer = self
        self.setPos(self.emap.endPos.x, self.emap.endPos.y)
        utility.rm.setMap(self.emap)

    def setEmapN(self, emap):
        self.emap = emap
        self.emap.addMob(self)
        self.emap.fovPlayer = self
        self.setPos(self.emap.startPos.x, self.emap.startPos.y)
        utility.rm.setMap(self.emap)

    def move(self, dx, dy):
        """
        Use this for mob movement
        """
        tempX = self.pos.x + dx
        tempY = self.pos.y + dy
        if tempX < 0 or tempX >= self.emap.width: return
        if tempY < 0 or tempY >= self.emap.height: return
        tile = self.emap.getTile(vec2.Vec2(tempX, tempY))
        if tile.blocking:
            tile.bump()
            return
        mob = self.emap.getMobs(vec2.Vec2(tempX, tempY))
        if mob is not None:
            self.attack(mob)
            return
        self.emap.resetProperty(self)
        self.pos.x = tempX
        self.pos.y = tempY
        self.emap.setProperty(self)
        tile.steppedOn()
        self.moves += 1
        items = self.emap.getItems(self.pos)
        itemList = []
        for i in items:
            if i.pickupable:
                itemList.append(i)
        if len(itemList) > 0:
            if len(itemList) == 1:
                ui.appendln("There " + self.aOrSome(items[0].name) + items[0].name + " here.")
            else: # more than one
                ui.appendln("There is a pile of items here.")

    def update(self):
        if self.dead:
            self.die()

        if self.moves >= self.speed:
            self.ableToMove = False
            return
        else:
            self.ableToMove = True

        if events.mode == events.modes["Default"]:
            self.handleLeveling()
            self.handleEvents()
        elif events.mode == events.modes["Inventory"]:
            self.inventoryWindow.draw()
        elif events.mode == events.modes["Help"]:
            self.helpWindow.draw()
        elif events.mode == events.modes["Examine"]:
            self.examineWindow.draw()
        elif events.mode == events.modes["RestingUntilDone"]:
            self.handleRestUntilDone()
        elif events.mode == events.modes["RestingForTurns"]:
            self.handleRestTurns()

    def draw(self):
        libtcod.console_set_default_background(0, self.backgroundColor)
        libtcod.console_set_default_foreground(0, self.color)
        libtcod.console_put_char(0, self.pos.x, self.pos.y, self.char, libtcod.BKGND_SET)

    def handleInventory(self):
        if len(self.inventory) == 0:
            ui.appendln("Your pockets are empty.")
            events.turnModeOff()
            return
        self.inventoryWindow.setTitle("Inventory")
        self.inventoryWindow.setList(self.inventory)

    def getAmmoTypeInInventory(self):
        for i in self.inventory:
            if i.objectType == object.objectTypes["Equippable"]:
                if i.eType == equippable.equippableTypes["Ammunition"]:
                    if i.ammoType == self.ammoType:
                        return i
        return None

    def handlePickup(self):
        items = self.emap.getItems(self.pos)
        itemList = []
        for i in items:
            if i.pickupable:
                itemList.append(i)
        if len(itemList) == 0:
            ui.appendln("You see no items to pickup.")
            events.turnModeOff()
            return
        if len(itemList) == 1:
            if not itemList[0].pickupable:
                ui.appendln("You see no items to pickup.")
                events.turnModeOff()
                return
            self.pickupItem(itemList[0])
            events.turnModeOff()
            return
        else:
            self.inventoryWindow.setList(itemList)
            found, index = self.inventoryWindow.wait()
            if found == -1:
                events.turnModeOff()
            elif found:
                self.pickupItem(itemList[index])
            events.turnModeOff()

    def pickupItem(self, item):
        if not item.pickupable:
            ui.appendln("You cannot pickup that item!")
            return
        if self.inventorySpace + item.space >= 24:
            ui.appendln("You cannot carry that much!")
            return
        if len(self.inventory) == 24:
            ui.appendln("You cannot carry anymore!")
            return
        ui.appendln("You pickup " + self.theOrSame(item.name) + item.name + ".")
        self.emap.removeObj(item)
        self.stackItems(item)
        self.inventory.append(item)

    def handleDropping(self):
        if len(self.inventory) == 0:
            ui.appendln("Your pockets are empty.")
            events.turnModeOff()
            return
        if len(self.inventory) == 1:
            self.dropItem(self.inventory[0])
            events.turnModeOff()
        else:
            self.inventoryWindow.setList(self.inventory)
            found, index = self.inventoryWindow.wait()
            if found == -1:
                events.turnModeOff()
            elif found:
                self.dropItem(self.inventory[index])
            events.turnModeOff()

    def lookMove(self, pos, dx, dy):
        tempX = pos.x + dx
        tempY = pos.y + dy
        if not libtcod.map_is_in_fov(self.emap.fov, tempX, tempY): return False, pos
        if tempX < 0 or tempX > self.emap.width: return False, pos
        if tempY < 0 or tempY > self.emap.height: return False, pos
        return True, vec2.Vec2(tempX, tempY)

    def handleLooking(self):
        lookPos = vec2.Vec2(self.pos.x, self.pos.y)
        while events.mode == events.modes["Looking"]:
            self.emap.draw()
            ui.draw()
            libtcod.console_set_default_foreground(0, libtcod.yellow)
            libtcod.console_print(0, 0, 0, "LOOKING")
            libtcod.console_put_char(0, lookPos.x, lookPos.y, '@')
            libtcod.console_flush()
            events.update()
            key = events.key
            if key.vk == libtcod.KEY_KP8:
                move, lookPos = self.lookMove(lookPos, 0, -1)
                if move:
                    self.displayDescription(lookPos)
            if key.vk == libtcod.KEY_KP2:
                move, lookPos = self.lookMove(lookPos, 0, 1)
                if move:
                    self.displayDescription(lookPos)
            if key.vk == libtcod.KEY_KP4:
                move, lookPos = self.lookMove(lookPos, -1, 0)
                if move:
                    self.displayDescription(lookPos)
            if key.vk == libtcod.KEY_KP6:
                move, lookPos = self.lookMove(lookPos, 1, 0)
                if move:
                    self.displayDescription(lookPos)
            if key.vk == libtcod.KEY_KP9:
                move, lookPos = self.lookMove(lookPos, 1, -1)
                if move:
                    self.displayDescription(lookPos)
            if key.vk == libtcod.KEY_KP7:
                move, lookPos = self.lookMove(lookPos, -1, -1)
                if move:
                    self.displayDescription(lookPos)
            if key.vk == libtcod.KEY_KP1:
                move, lookPos = self.lookMove(lookPos, -1, 1)
                if move:
                    self.displayDescription(lookPos)
            if key.vk == libtcod.KEY_KP3:
                move, lookPos = self.lookMove(lookPos, 1, 1)
                if move:
                    self.displayDescription(lookPos)

    def displayDescription(self, pos):
        tile = self.emap.getTile(pos)
        ui.appendln("You see " + tile.desc + ".")
        items = self.emap.getItems(pos)
        itemList = []
        for i in items:
            if i.pickupable:
                itemList.append(i)
        if len(itemList) > 0:
            if len(itemList) == 1:
                ui.appendln("There " + self.aOrSome(items[0].name) + items[0].name + ".")
            else: # more than one
                ui.appendln("There is a pile of items here.")
        mob = self.emap.getMobs(pos)
        if mob is not None:
            if mob.name == self.name:
                return
            ui.appendln("There is " + self.aOrAn(mob.name) + mob.name + ".")

    def aOrAn(self, string):
        if string[0] == 'o' or string[0] == 'O':
            return "an "
        return 'a '

    def aOrSome(self, string):
        if string == "Plate Cuirass": return 'a '
        if string[len(string) - 1] == 's':
            return "are some "
        return 'is a '

    def theOrSame(self, string):
        if string == "Plate Cuirass": return 'a '
        if string[-1] == 's':
            return "some "
        return "the "

    def initHelp(self):
        self.helpWindow.appendln("Actions")
        self.helpWindow.appendln("=======")
        self.helpWindow.appendln("Use the numpad keys to move.")
        self.helpWindow.appendln("Numpad 5 - Rest for a turn")
        self.helpWindow.appendln("")
        self.helpWindow.appendln("i - Inventory")
        self.helpWindow.appendln("e - Equip Item")
        self.helpWindow.appendln("E - Examine Item")
        self.helpWindow.appendln("d - Drop Item")
        self.helpWindow.appendln("p - Pickup Item")
        self.helpWindow.appendln("l - Look Around")
        self.helpWindow.appendln("f - Fire Ranged Weapon")
        self.helpWindow.appendln("r - Rest until fully healed")
        self.helpWindow.appendln("R - Rest for number of turns")
        self.helpWindow.appendln("u - Use object on ground")
        self.helpWindow.appendln("? - Help Menu")
        self.helpWindow.appendln("")
        self.helpWindow.appendln("ESC - Stop Action")

    def useAction(self):
        tile = self.emap.getTile(self.pos)
        if tile.use():
            self.moves += 1

    def handleEvents(self):
        key = events.key
        if key.vk == libtcod.KEY_KP8 or key.c == ord('8'):
            self.move(0, -1)
        if key.vk == libtcod.KEY_KP2 or key.c == ord('2'):
            self.move(0, 1)
        if key.vk == libtcod.KEY_KP4 or key.c == ord('4'):
            self.move(-1, 0)
        if key.vk == libtcod.KEY_KP6 or key.c == ord('6'):
            self.move(1, 0)
        if key.vk == libtcod.KEY_KP9 or key.c == ord('9'):
            self.move(1, -1)
        if key.vk == libtcod.KEY_KP7 or key.c == ord('7'):
            self.move(-1, -1)
        if key.vk == libtcod.KEY_KP1 or key.c == ord('1'):
            self.move(-1, 1)
        if key.vk == libtcod.KEY_KP3 or key.c == ord('3'):
            self.move(1, 1)
        if key.vk == libtcod.KEY_KP5 or key.c == ord('5'):
            if self.hp == self.maxhp and self.stam == self.maxstam:
                self.ableToMove = False
            else:
                self.rest()
        if key.c == ord('i'):
            events.turnModeOn(events.modes["Inventory"])
            self.handleInventory()
        if key.c == ord('e'):
            events.turnModeOn(events.modes["Equipping"])
            self.inventoryWindow.name = "Equipping"
            self.handleEquipping()
        if key.c == ord('p'):
            events.turnModeOn(events.modes["PickingUp"])
            self.inventoryWindow.name = "Picking Up"
            self.handlePickup()
        if key.c == ord('d'):
            events.turnModeOn(events.modes["Dropping"])
            self.inventoryWindow.name = "Dropping"
            self.handleDropping()
        if key.c == ord('l'):
            events.turnModeOn(events.modes["Looking"])
            self.handleLooking()
        if key.c == ord('?'):
            events.turnModeOn(events.modes["Help"])
        if key.c == ord('E'):
            events.turnModeOn(events.modes["Examine"])
            self.handleExamine()
        if key.c == ord('R'):
            if self.hp == self.maxhp and self.stam == self.maxstam:
                ui.appendln("You're already at full health!")
                return
            events.turnModeOn(events.modes["RestingForTurns"])
            self.totalRestTurns = prompt.waitNumber("How many turns to rest? ")
            self.restTurns = 0
            ui.appendln("Resting.. ")
            self.handleRestTurns()
        if key.c == ord('r'):
            if self.hp == self.maxhp and self.stam == self.maxstam:
                ui.appendln("You're already at full health!")
                return
            events.turnModeOn(events.modes["RestingUntilDone"])
            ui.appendln("Resting.. ")
            self.handleRestUntilDone()
        if key.c == ord('u'): # use
            self.useAction()
        if key.c == ord('f'):
            events.turnModeOn(events.modes["Firing"])
            self.handleFiring()
            #if key.c == ord('b'): # defend for a turn
            #    self.defend()

    def dropItem(self, item):
        if item.equipped:
            self.equip(item) # unequips the item properly
        ui.appendln("You drop " + self.theOrSame(item.name) + item.name + ".")
        item.setPos(self.pos.x, self.pos.y)
        self.emap.addObj(item)
        self.inventory.remove(item)

    def stackItems(self, item):
        if not item.stackable: return
        for i in self.inventory:
            if i.stackable:
                if i.singularName == item.singularName:
                    if item.level == i.level:
                        if i.equipped:
                            self.equip(item)
                        item.stack(i.amount)
                        self.inventory.remove(i)

    def handleEquipping(self):
        if len(self.inventory) == 0:
            ui.appendln("You have nothing to equip!")
            events.turnModeOff()
            return
        elif len(self.inventory) == 1:
            if self.inventory[0].equipped:
                self.equip(self.inventory[0])
                events.turnModeOff()
                return
            else:
                self.equip(self.inventory[0])
        else:
            self.inventoryWindow.setList(self.inventory) # make sure list is on inventory
            found, index = self.inventoryWindow.wait()
            if found == -1:
                events.turnModeOff()
            elif found:
                self.equip(self.inventory[index])
            events.turnModeOff()

    def wearingArmor(self, armorType):
        for i in self.inventory:
            if i.objectType == object.objectTypes["Equippable"]:
                if i.equipped:
                    if i.eType == equippable.equippableTypes["Armor"]:
                        if i.armorType == armorType:
                            return True
        return False

    def wearingWeapon(self):
        for i in self.inventory:
            if i.objectType == object.objectTypes["Equippable"]:
                if i.equipped:
                    if i.eType == equippable.equippableTypes["Weapon"]:
                        return True, i
        return False, None

    def wearingShield(self):
        for i in self.inventory:
            if i.objectType == object.objectTypes["Equippable"]:
                if i.equipped:
                    if i.eType == equippable.equippableTypes["Shield"]:
                        return True
        return False

    def wearingAmmunition(self):
        for i in self.inventory:
            if i.objectType == object.objectTypes["Equippable"]:
                if i.equipped:
                    if i.eType == equippable.equippableTypes["Ammunition"]:
                        return True, i
        return False, None

    def canEquip(self, item):
        if item.objectType != object.objectTypes["Equippable"]:
            ui.appendln("You cannot equip that!")
            return False
        if item.eType == equippable.equippableTypes["Weapon"]:
            wearingWeapon, weapon = self.wearingWeapon()
            if self.wearingShield():
                if item.reqs.twoH:
                    ui.appendln("You cannot equip that with a shield!")
                    return False
            if wearingWeapon:
                ui.appendln("You cannot equip that!")
                return False
        elif item.eType == equippable.equippableTypes["Armor"]:
            wearingArmor = self.wearingArmor(item.armorType)
            if wearingArmor: # True if same armor type
                ui.appendln("You cannot equip that!")
                return False
        elif item.eType == equippable.equippableTypes["Shield"]:
            wearingShield = self.wearingShield()
            wearingWeapon, weapon = self.wearingWeapon()
            if wearingShield:
                ui.appendln("You cannot equip that!")
                return False
            if wearingWeapon:
                if weapon.reqs.twoH:
                    ui.appendln("You cannot equip that!")
                    return False
        elif item.eType == equippable.equippableTypes["Ammunition"]:
            wearingAmmo, ammo = self.wearingAmmunition()
            if wearingAmmo:
                if ammo.ammoType != item.ammoType:
                    ui.appendln("You cannot equip that!")
                    return False
        reqs = item.reqs
        if self.str < reqs.str:
            ui.appendln("You do not have enough strength!")
            return False
        if self.dex < reqs.dex:
            ui.appendln("You do not have enough dexterity!")
            return False
        if self.int < reqs.int:
            ui.appendln("You do not have enough intelligence!")
            return False
        if self.wpr < reqs.wpr:
            ui.appendln("You do not have enough willpower!")
            return False
        return True

    def equip(self, item):
        if item.equipped:
            ui.appendln("You unequip " + self.theOrSame(item.name) + item.name + ".")
            item.equipped = False
            self.str -= item.stats.str
            self.dex -= item.stats.dex
            self.int -= item.stats.int
            self.wpr -= item.stats.wpr
            self.perception -= item.stats.per
            self.speed -= item.stats.spd
            self.poisonResist -= item.stats.rP
            self.fireResist -= item.stats.rF
            self.coldResist -= item.stats.rC
            self.taintResist -= item.stats.rT
            self.armorClass -= item.stats.ac
            self.wepmindmg -= item.stats.minDmg
            self.wepmaxdmg -= item.stats.maxDmg
            self.blockChance -= item.stats.blockChance
            self.stamUse -= item.reqs.stam
            if item.eType == equippable.equippableTypes["Weapon"]:
                if item.weaponType == equippable.weaponTypes["Ranged"]:
                    self.hasRangedWep = False
                    self.ammoType = -1
            self.updateStats()
        else:
            if self.canEquip(item):
                ui.appendln("You equip " + self.theOrSame(item.name) + item.name + ".")
                item.equipped = True
                self.str += item.stats.str
                self.dex += item.stats.dex
                self.int += item.stats.int
                self.wpr += item.stats.wpr
                self.perception += item.stats.per
                self.speed += item.stats.spd
                self.poisonResist += item.stats.rP
                self.fireResist += item.stats.rF
                self.coldResist += item.stats.rC
                self.taintResist += item.stats.rT
                self.armorClass += item.stats.ac
                self.wepmindmg += item.stats.minDmg
                self.wepmaxdmg += item.stats.maxDmg
                self.blockChance += item.stats.blockChance
                self.stamUse += item.reqs.stam
                if item.eType == equippable.equippableTypes["Weapon"]:
                    if item.weaponType == equippable.weaponTypes["Ranged"]:
                        self.hasRangedWep = True
                        self.ammoType = item.reqs.amm
                self.updateStats()
        events.turnModeOff()

    def handleExamine(self):
        if len(self.inventory) == 0:
            ui.appendln("Your pockets are empty.")
            events.turnModeOff()
            return
        if len(self.inventory) == 1:
            self.examineItem(self.inventory[0])
        else:
            self.inventoryWindow.setList(self.inventory)
            self.inventoryWindow.setTitle("Inventory")
            found, index = self.inventoryWindow.wait()
            if found == -1:
                events.turnModeOff()
            elif found:
                self.examineItem(self.inventory[index])
            else:
                events.turnModeOff()

    def examineItem(self, item):
        self.examineWindow.reset()
        self.examineWindow.setTitle(item.name)
        objectType = ""
        for t, n in object.objectTypes.iteritems():
            if n == item.objectType:
                objectType = t
        self.examineWindow.appendln(objectType)
        if item.objectType == object.objectTypes["Equippable"]:
            eType = ""
            for t, n in equippable.equippableTypes.iteritems():
                if n == item.eType:
                    eType = t
            self.examineWindow.appendln(eType)
            if item.eType == equippable.equippableTypes["Armor"]:
                aType = ""
                for t, n in equippable.armorTypes.iteritems():
                    if n == item.armorType:
                        aType = t
                self.examineWindow.appendln(aType)
            if item.eType == equippable.equippableTypes["Weapon"]:
                if item.reqs.twoH:
                    self.examineWindow.appendln("Two Handed", 15, self.examineWindow.getyPos("Weapon"), False,
                                                libtcod.desaturated_red)
            self.examineWindow.appendln("")
            self.examineWindow.appendln("Stats:")
            printNone = True
            if item.stats.ac != 0:
                printNone = False
                self.examineWindow.appendln("AC: " + str(item.stats.ac))
            if item.stats.blockChance != 0:
                printNone = False
                self.examineWindow.appendln("BLOCK: " + str(item.stats.blockChance))
            if item.stats.maxDmg != 0:
                printNone = False
                self.examineWindow.appendln("DMG: " + str(item.stats.minDmg) + " - " + str(item.stats.maxDmg))
            if item.stats.str != 0:
                printNone = False
                self.examineWindow.appendln("STR: " + str(item.stats.str))
            if item.stats.dex != 0:
                printNone = False
                self.examineWindow.appendln("DEX: " + str(item.stats.dex))
            if item.stats.int != 0:
                printNone = False
                self.examineWindow.appendln("INT: " + str(item.stats.int))
            if item.stats.wpr != 0:
                printNone = False
                self.examineWindow.appendln("WPR: " + str(item.stats.wpr))
            if item.stats.per != 0:
                printNone = False
                self.examineWindow.appendln("PER: " + str(item.stats.per))
            if item.stats.spd != 0:
                printNone = False
                self.examineWindow.appendln("SPD: " + str(item.stats.spd))
            if item.stats.rP != 0:
                printNone = False
                self.examineWindow.appendln("rP: " + str(item.stats.rP))
            if item.stats.rF != 0:
                printNone = False
                self.examineWindow.appendln("rF: " + str(item.stats.rF))
            if item.stats.rC != 0:
                printNone = False
                self.examineWindow.appendln("rC: " + str(item.stats.rC))
            if item.stats.rT != 0:
                printNone = False
                self.examineWindow.appendln("rT: " + str(item.stats.rT))
            if printNone:
                self.examineWindow.appendln("None")

            self.examineWindow.appendln("Reqs:", 15, self.examineWindow.getyPos("Stats:"), False)
            printNone = True
            reqs = item.reqs
            if reqs.stam != 0:
                printNone = False
                color = libtcod.green
                if self.maxstam < reqs.stam:
                    color = libtcod.red
                self.examineWindow.appendln("SP >= " + str(reqs.stam), 15, 1, True, color)
            if reqs.str != 0:
                printNone = False
                color = libtcod.green
                if self.str < reqs.str:
                    color = libtcod.red
                self.examineWindow.appendln("STR >= " + str(reqs.str), 15, 1, True, color)
            if reqs.dex != 0:
                printNone = False
                color = libtcod.green
                if self.dex < reqs.dex:
                    color = libtcod.red
                self.examineWindow.appendln("DEX >= " + str(reqs.dex), 15, 1, True, color)
            if reqs.int != 0:
                printNone = False
                color = libtcod.green
                if self.int < reqs.int:
                    color = libtcod.red
                self.examineWindow.appendln("INT >= " + str(reqs.int), 15, 1, True, color)
            if reqs.wpr != 0:
                printNone = False
                color = libtcod.green
                if self.wpr < reqs.wpr:
                    color = libtcod.red
                self.examineWindow.appendln("WPR >= " + str(reqs.wpr), 15, 1, True, color)
            if printNone:
                self.examineWindow.appendln("None", 15)
        self.examineWindow.appendln("Space: " + str(item.space), 15, self.examineWindow.getyPos(objectType), False)

    #end examineItem()

    def defend(self):
        ui.appendln("IMPLEMENT DEFENDING", libtcod.red)
        pass

    def handleRestTurns(self):
        self.rest() # if done resting prematurely
        self.restTurns += 1
        if self.restTurns == self.totalRestTurns:
            ui.appendln("Done resting!")
            events.turnModeOff()

    def handleRestUntilDone(self):
        if self.rest():
            ui.appendln("Done resting!")
            events.turnModeOff()

    def heal(self, amount):
        self.hp += amount
        if self.hp > self.maxhp:
            self.hp = self.maxhp

    def healStam(self, amount):
        self.stam += amount
        if self.stam > self.maxstam:
            self.stam = self.maxstam

    def rest(self):
        self.moves += 1
        restHP = float(self.str) / 5
        restStam = float(self.dex) / 2
        self.heal(restHP)
        self.healStam(restStam)
        if self.hp == self.maxhp and self.stam == self.maxstam:
            return True
        return False

    def giveExp(self, amount):
        self.exp += amount
        self.handleLeveling()

    def handleLeveling(self):
        if self.exp >= self.maxexp:
            self.leveled()

    def leveled(self):
        diff = self.exp - self.maxexp
        self.level += 1
        self.maxexp *= self.expIncrease
        self.maxexp = int(self.maxexp)
        self.exp = diff
        ui.appendln("You are now level " + str(self.level) + "!", libtcod.green)
        attribPoints = 2
        curAP = 0
        while curAP != attribPoints:
            if curAP == 0:
                ui.appendln("You have " + str(attribPoints - curAP) + " points left.")
            else:
                ui.appendln("You have " + str(attribPoints - curAP) + " point left.")
            ans = prompt.waitLevel("Pick an attribute (s/d) ")
            if ans == 1:
                self.str += 1
            elif ans == 2:
                self.dex += 1
            elif ans == 3:
                self.int += 1
            elif ans == 4:
                self.wpr += 1
            self.updateStats()
            curAP += 1
        self.hp = self.maxhp
        self.stam = self.maxstam
        self.mp = self.maxmp

    def handleFiring(self):
        if not self.hasRangedWep:
            ui.appendln("You are not holding a ranged weapon!")
            events.turnModeOff()
            return
        lookPos = vec2.Vec2(self.pos.x, self.pos.y)
        timer = 0
        totalTimer = 60
        drawCrosshairs = True
        while events.mode == events.modes["Firing"]:
            timer += 1
            if timer >= totalTimer:
                drawCrosshairs = not drawCrosshairs
                timer = 0
            self.emap.draw()
            ui.draw()
            libtcod.console_set_default_foreground(0, libtcod.red)
            libtcod.console_print(0, 0, 0, "FIRING")
            if drawCrosshairs:
                libtcod.console_put_char(0, lookPos.x, lookPos.y, '+')
            libtcod.console_flush()
            events.update()
            key = events.key
            if key.vk == libtcod.KEY_ENTER:
                found, mob = self.testFire(lookPos)
                if found:
                    if mob.name == self.name:
                        ui.appendln("You cannot attack yourself!", libtcod.desaturated_yellow)
                        continue
                    if self.stam < self.stamUse:
                        ui.appendln("You are too tired too attack!", libtcod.yellow)
                        events.turnModeOff()
                        return
                    ammo = self.getAmmoTypeInInventory()
                    if ammo is not None:
                        ammo.stack(-1)
                        if ammo.amount <= 0:
                            self.inventory.remove(ammo)
                    else:
                        ui.appendln("You have no ammo!", libtcod.desaturated_yellow)
                        events.turnModeOff()
                        return
                    self.attack(mob)
                    events.turnModeOff()
                    return
                else:
                    ui.appendln("There is no monster there!", libtcod.desaturated_yellow)
            if key.vk == libtcod.KEY_KP8:
                move, lookPos = self.lookMove(lookPos, 0, -1)
            if key.vk == libtcod.KEY_KP2:
                move, lookPos = self.lookMove(lookPos, 0, 1)
            if key.vk == libtcod.KEY_KP4:
                move, lookPos = self.lookMove(lookPos, -1, 0)
            if key.vk == libtcod.KEY_KP6:
                move, lookPos = self.lookMove(lookPos, 1, 0)
            if key.vk == libtcod.KEY_KP9:
                move, lookPos = self.lookMove(lookPos, 1, -1)
            if key.vk == libtcod.KEY_KP7:
                move, lookPos = self.lookMove(lookPos, -1, -1)
            if key.vk == libtcod.KEY_KP1:
                move, lookPos = self.lookMove(lookPos, -1, 1)
            if key.vk == libtcod.KEY_KP3:
                move, lookPos = self.lookMove(lookPos, 1, 1)

    def testFire(self, pos):
        mob = self.emap.getMobs(pos)
        if mob is None: return False, None
        return True, mob

    def missed(self):
        ui.appendln("You missed!")

    def hit(self, amount, mob):
        ui.appendln("You hit the " + mob.name + " for " + str(amount) + " damage.", libtcod.desaturated_green)

    def tired(self):
        ui.appendln("You are too tired to attack!", libtcod.yellow)

    def dodged(self, mob):
        ui.appendln("You dodged the " + mob.name + "\'s attack.")

    def blocked(self, mob):
        ui.appendln("You blocked the " + mob.name + "\'s attack.")

    def die(self):
        events.turnModeOn("Dead")
        ui.appendln("You died!", libtcod.red)
        restart = prompt.wait("Restart? ", libtcod.red)
        while restart == -1:
            restart = prompt.wait("Restart? ", libtcod.red)
        if restart:
            utility.restart()
        else:
            events.exitGame()
