__author__ = 'darithorn'

import libtcodpy as libtcod
import mob

class Skeleton(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'S', libtcod.white, emap,
                               "no desc",
                               "skeleton", spawner)
        self.speed = 1
        self.level = 1
        self.str = 2
        self.dex = 3
        self.wepmindmg = 1
        self.wepmaxdmg = 3

class Spectre(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 's', libtcod.dark_gray, emap,
                               "no desc",
                               "spectre", spawner)
        self.speed = 1
        self.level = 2
        self.armorClass = 2
        self.str = 2
        self.dex = 5
        self.wepmindmg = 1
        self.wepmaxdmg = 3
        self.mCmin = 0
        self.mCmax = 2

class Zombie(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'Z', libtcod.desaturated_green, emap,
                               "no desc",
                               "zombie", spawner)
        self.speed = 1
        self.level = 3
        self.armorClass = 4
        self.str = 3
        self.dex = 6
        self.wepmindmg = 2
        self.wepmaxdmg = 5

class Mummy(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'M', libtcod.light_yellow, emap,
                               "no desc",
                               "mummy", spawner)
        self.speed = 1
        self.level = 4
        self.armorClass = 4
        self.str = 7
        self.dex = 5
        self.wepmindmg = 3
        self.wepmaxdmg = 7

class Lich(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'L', libtcod.gray, emap,
                               "no desc",
                               "lich", spawner)
        self.speed = 1
        self.level = 5
        self.armorClass = 4
        self.str = 8
        self.dex = 7
        self.wepmindmg = 5
        self.wepmaxdmg = 12
        self.mCmin = 5
        self.mCmax = 9
        self.attackRange = 5

class Goblin(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'g', libtcod.dark_green, emap,
                               "no desc",
                               "goblin", spawner)
        self.speed = 3
        self.level = 6
        self.armorClass = 3
        self.str = 5
        self.dex = 7
        self.wepmindmg = 3
        self.wepmaxdmg = 6

class OrcWarrior(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'o', libtcod.desaturated_green, emap,
                               "no desc",
                               "orc warrior", spawner)
        self.speed = 1
        self.level = 7
        self.armorClass = 6
        self.str = 9
        self.dex = 7
        self.wepmindmg = 5
        self.wepmaxdmg = 9

class GoblinShaman(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'g', libtcod.desaturated_flame, emap,
                               "no desc",
                               "goblin shaman", spawner)
        self.speed = 1
        self.level = 8
        self.armorClass = 4
        self.str = 4
        self.dex = 10
        self.wepmindmg = 1
        self.wepmaxdmg = 1
        self.mFmin = 6
        self.mFmax = 10
        self.attackRange = 5

class Troll(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'T', libtcod.green, emap,
                               "no desc",
                               "troll", spawner)
        self.speed = 1
        self.level = 9
        self.armorClass = 3
        self.str = 10
        self.dex = 7
        self.wepmindmg = 8
        self.wepmaxdmg = 20
        self.attackRange = 3

class OrcWarlord(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'o', libtcod.yellow, emap,
                               "no desc",
                               "orc warlord", spawner)
        self.speed = 1
        self.level = 10
        self.armorClass = 6
        self.str = 13
        self.dex = 8
        self.wepmindmg = 9
        self.wepmaxdmg = 14

class RatmanRogue(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'r', libtcod.sepia, emap,
                               "no desc",
                               "ratman rogue", spawner)
        self.speed = 5
        self.level = 11
        self.armorClass = 4
        self.str = 10
        self.dex = 12
        self.wepmindmg = 7
        self.wepmaxdmg = 12
        self.mPmin = 2
        self.mPmax = 7

class RatmanSeer(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'r', libtcod.lightest_gray, emap,
                               "no desc",
                               "ratman seer", spawner)
        self.speed = 1
        self.level = 12
        self.armorClass = 4
        self.str = 10
        self.dex = 10
        self.wepmindmg = 3
        self.wepmaxdmg = 6
        self.mFmin = 4
        self.mFmax = 9
        self.mPmin = 6
        self.mPmax = 11
        self.attackRange = 5

class RatmanEngineer(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'r', libtcod.light_yellow, emap,
                               "no desc",
                               "ratman engineer", spawner)
        self.speed = 1
        self.level = 13
        self.armorClass = 5
        self.str = 10
        self.dex = 11
        self.wepmindmg = 11
        self.wepmaxdmg = 18
        self.mPmin = 5
        self.mPmax = 10
        self.attackRange = 3

class Beastman(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'b', libtcod.light_sepia, emap,
                               "no desc",
                               "beastman", spawner)
        self.speed = 1
        self.level = 14
        self.armorClass = 6
        self.str = 13
        self.dex = 10
        self.wepmindmg = 12
        self.wepmaxdmg = 20
        self.attackRange = 1

class RatOgre(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'R', libtcod.sepia, emap,
                               "no desc",
                               "rat ogre", spawner)
        self.speed = 1
        self.level = 15
        self.armorClass = 4
        self.str = 15
        self.dex = 9
        self.wepmindmg = 12
        self.wepmaxdmg = 23
        self.attackRange = 1

class TaintedDwarf(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 't', libtcod.light_amber, emap,
                               "no desc",
                               "tainted dwarf", spawner)
        self.speed = 1
        self.level = 16
        self.armorClass = 5
        self.str = 13
        self.dex = 10
        self.wepmindmg = 13
        self.wepmaxdmg = 19
        self.attackRange = 1

class Succubus(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 's', libtcod.white, emap,
                               "no desc",
                               "succubus", spawner)
        self.speed = 1
        self.level = 17
        self.armorClass = 5
        self.str = 12
        self.dex = 15
        self.wepmindmg = 11
        self.wepmaxdmg = 18
        self.mTmin = 6
        self.mTmax = 12
        self.attackRange = 2

class Drow(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'd', libtcod.black, emap,
                               "no desc",
                               "drow", spawner)
        self.speed = 1
        self.level = 18
        self.armorClass = 4
        self.str = 10
        self.dex = 14
        self.wepmindmg = 1
        self.wepmaxdmg = 1
        self.mFmin = 15
        self.mFmax = 23
        self.attackRange = 4

class ForgottenOne(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'f', libtcod.desaturated_yellow, emap,
                               "no desc",
                               "forgotten one", spawner)
        self.speed = 1
        self.level = 19
        self.armorClass = 6
        self.str = 15
        self.dex = 14
        self.wepmindmg = 13
        self.wepmaxdmg = 20
        self.mTmin = 12
        self.mTmax = 20
        self.attackRange = 1

class Demon(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'D', libtcod.red, emap,
                               "no desc",
                               "demon", spawner)
        self.speed = 1
        self.level = 25
        self.armorClass = 8
        self.str = 25
        self.dex = 15
        self.wepmindmg = 13
        self.wepmaxdmg = 26
        self.mFmin = 16
        self.mFmax = 24
        self.attackRange = 1

class Dragon(mob.NormalMob):
    def __init__(self, pos, emap, spawner=False):
        mob.NormalMob.__init__(self, pos, 'D', libtcod.green, emap,
                               "no desc",
                               "dragon", spawner)
        self.speed = 1
        self.level = 30
        self.armorClass = 13
        self.str = 22
        self.dex = 17
        self.wepmindmg = 25
        self.wepmaxdmg = 33
        self.mFmin = 20
        self.mFmax = 28
        self.attackRange = 1

    def die(self):
        import window
        window.WonGameWindow()