__author__ = 'darithorn'

import libtcodpy as libtcod
import ui


def wait(text, color=libtcod.lightest_gray, won=False):
    ui.append(text, color, won)
    libtcod.console_flush()
    key = libtcod.console_check_for_keypress()
    while key.c is not ord('y') or key.c is not ord('n'):
        key = libtcod.console_check_for_keypress()
        if key.c == ord('y'):
            ui.appendln(chr(key.c), won)
            return True
        elif key.c == ord('n'):
            ui.appendln(chr(key.c), won)
            return False
        elif key.vk == libtcod.KEY_ESCAPE:
            ui.appendln("", won)
            return -1

def waitLevel(text, color=libtcod.lightest_gray):
    ui.append(text, color)
    libtcod.console_flush()
    key = libtcod.console_check_for_keypress()
    while key.c is not ord('s') or key.c is not ord('d'):
        key = libtcod.console_check_for_keypress()
        if key.c == ord('s'):
            ui.appendln(chr(key.c))
            return 1
        elif key.c == ord('d'):
            ui.appendln(chr(key.c))
            return 2

def waitNumber(text):
    ui.append(text)
    libtcod.console_flush()
    key = libtcod.console_check_for_keypress()
    inp = ''
    while key.vk is not libtcod.KEY_ENTER:
        key = libtcod.console_check_for_keypress()
        if key.vk == libtcod.KEY_ESCAPE:
            ui.appendln('0')
            return -1
        if key.c != ord(' '):
            if chr(key.c).isdigit():
                ui.remove(inp)
                inp += chr(key.c)
                ui.appendln(inp)
                libtcod.console_flush()
        if key.vk == libtcod.KEY_BACKSPACE:
            count = 0
            newInp = ''
            while count < len(inp) - 1:
                newInp += inp[count]
                count += 1
            ui.remove(inp)
            inp = newInp
            ui.appendln(inp)
            libtcod.console_flush()

    if inp == '':
        ui.appendln('0')
        inp = 0
    return int(inp)