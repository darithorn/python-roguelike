__author__ = 'darithorn'

import libtcodpy as libtcod
import equippable


class Buckler(equippable.Shield):
    def __init__(self, emap, spawner=False):
        equippable.Shield.__init__(self, libtcod.desaturated_amber, emap,
                                   "no desc",
                                   "Buckler",
                                   equippable.Requirements(str=1),
                                   equippable.Stats(ac=1, blockChance=3),
                                   spawner=spawner)


class WoodenShield(equippable.Shield):
    def __init__(self, emap, spawner=False):
        equippable.Shield.__init__(self, libtcod.desaturated_amber, emap,
                                   "no desc",
                                   "Wooden Shield",
                                   equippable.Requirements(str=4),
                                   equippable.Stats(dex=-1, ac=2, blockChance=5),
                                   spawner=spawner)


class Rondache(equippable.Shield):
    def __init__(self, emap, spawner=False):
        equippable.Shield.__init__(self, libtcod.desaturated_amber, emap,
                                   "no desc",
                                   "Rondache",
                                   equippable.Requirements(str=8),
                                   equippable.Stats(dex=-1, ac=3, blockChance=8),
                                   spawner=spawner)


class TearKiteShield(equippable.Shield):
    def __init__(self, emap, spawner=False):
        equippable.Shield.__init__(self, libtcod.desaturated_amber, emap,
                                   "no desc",
                                   "Tear Kite Shield",
                                   equippable.Requirements(str=13),
                                   equippable.Stats(dex=-2, ac=4, blockChance=12),
                                   spawner=spawner)


class HeaterShield(equippable.Shield):
    def __init__(self, emap, spawner=False):
        equippable.Shield.__init__(self, libtcod.desaturated_amber, emap,
                                   "no desc",
                                   "Heater Shield",
                                   equippable.Requirements(str=17),
                                   equippable.Stats(dex=-3, ac=6, blockChance=15),
                                   spawner=spawner)